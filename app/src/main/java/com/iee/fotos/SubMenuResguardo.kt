package com.iee.fotos

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class SubMenuResguardo : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_submenu_resguardo)

        val Capitalizablebtn = findViewById<Button>(R.id.button4)
        val NoCapbtn = findViewById<Button>(R.id.button5)

        Capitalizablebtn.setOnClickListener {
            val intent = Intent(this,ListaResguardoActivity::class.java)
            startActivity(intent)
        }

        NoCapbtn.setOnClickListener {
            val intent = Intent(this,ListaResguardoActivity::class.java)
            startActivity(intent)
        }

    }

}