package com.iee.fotos

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.AsyncTask
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.iee.fotos.inventario.Edificiom

class ResguardoActivity : AppCompatActivity() {

    lateinit var apiInterface: APIInterface
    lateinit var idarray: IntArray
    lateinit var EditNoEmp: EditText
    lateinit var EditNombre: EditText
    lateinit var EditArea: EditText
    lateinit var EditDepto: EditText
    lateinit var EditPuesto: EditText
    lateinit var Buscarbtn: ImageButton
    lateinit var Continuarbtn: Button
    var adapter: ArrayAdapter<String>? = null
    lateinit var datos: SharedPreferences
    var filename = "datos"
    var bandera = 0
    var IDBien = 0
    var nombre = ""
    var edifico = ""
    var depto = ""
    var iduser = ""
    var iduserint = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alta_resguardo)

        apiInterface = APIClient.getClient().create(APIInterface::class.java)
        datos = getSharedPreferences(filename, 0)
        iduser = datos.getString("iduser", "").toString()
        iduserint = iduser.toInt()

        EditNoEmp = findViewById<EditText>(R.id.NoEmpleado)
        EditNombre = findViewById<EditText>(R.id.Nombretxt)
        EditArea = findViewById<EditText>(R.id.Areatxt)
        EditDepto = findViewById<EditText>(R.id.Dptotxt)
        EditPuesto = findViewById<EditText>(R.id.Puestotxt)
        Buscarbtn = findViewById<ImageButton>(R.id.imageButtonBuscar)
        Continuarbtn = findViewById<Button>(R.id.button3)

        Buscarbtn.setOnClickListener {
            if(EditNoEmp.text.toString() == "")
            {
                AlertDialog.Builder(this)
                        .setTitle("Campo Vacio")
                        .setMessage("Debes ingresar un numero de empleado")
                        .setNeutralButton("Aceptar") { dialog, whichButton ->
                            dialog.dismiss()
                            limpiarCampos()
                        }
                        .show()
            }
            else
            {
                val noempleado = Integer.parseInt(EditNoEmp.text.toString());
                ObtenerEmpleado(noempleado)
            }
        }

        Continuarbtn.setOnClickListener {
            if (RevizarVacio(bandera)){
                val noempleado = Integer.parseInt(EditNoEmp.text.toString());
                nombre = EditNombre.text.toString()
                depto = EditDepto.text.toString()
                val area = EditArea.text.toString()
                val puesto = EditPuesto.text.toString()
                AgregarResguardo(noempleado,nombre,area,depto,iduserint,puesto)
            }
        }

    }

    override fun onStart() {
        super.onStart()
    }

    private fun RevizarVacio(ID : Int) : Boolean{
        var pase = false
        if(ID == 0)
        {
            AlertDialog.Builder(this)
                    .setTitle("Empleado no Encontrado")
                    .setMessage("Debes ingresar un nuevo de empleado antes de continuar")
                    .setNeutralButton("Aceptar") { dialog, whichButton ->
                        dialog.dismiss()
                    }
                    .show()
        }
        else
        {
            pase = true
        }
        return pase
    }

    private fun limpiarCampos(){
        EditNombre.setText("")
        EditNoEmp.setText("")
        EditPuesto.setText("")
        EditDepto.setText("")
        EditArea.setText("")
        bandera = 0
    }

    fun ObtenerEmpleado(noempleado: Int){

        doAsync{
            val call = apiInterface.doGetEmpleadoResg(noempleado)
            val result = call.execute().body()
            val nombre = result?.nombre.toString()
            val area = result?.areadesc.toString()
            val depto = result?.deptodesc.toString()
            val puesto = result?.puesto.toString()
            bandera = result?.empleadosID!!.toInt()

            runOnUiThread {
                EditNombre.setText(nombre)
                EditArea.setText(area)
                EditDepto.setText(depto)
                EditPuesto.setText(puesto)
            }

        }

    }

    fun AgregarResguardo(noempleado: Int,nombrempleado: String ,area: String,depto: String,usuarioid : Int,puesto: String) {
        doAsync{
            val call = apiInterface.doSetResguardo(noempleado,nombrempleado,area,depto,usuarioid,puesto)
            val result = call.execute().body()
            val ID = result?.status.toString();
            val IDEntero = ID.toInt()

            val view = Intent(this, ListaDetalleResguardo::class.java)
            view.putExtra("nombre",nombrempleado)
            view.putExtra("depto",depto)
            view.putExtra("area",area)
            view.putExtra("ResguardoID",IDEntero)
            startActivity(view)
            finish()
        }
    }


    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        init {
            execute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
        }
    }


}