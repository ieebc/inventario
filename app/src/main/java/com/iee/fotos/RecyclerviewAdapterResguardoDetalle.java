package com.iee.fotos;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.iee.fotos.inventario.ArticuloResguardo;
import java.util.List;

public class RecyclerviewAdapterResguardoDetalle extends RecyclerView
        .Adapter<RecyclerviewAdapterResguardoDetalle
        .DataObjectHolder>{
    private static String LOG_TAG = "RecyclerviewAdapterResguardoDetalle";
    private List<ArticuloResguardo> mDataset;
    private static RecyclerviewAdapterResguardoDetalle.MyClickListener myClickListener;

    public static String filename = "datos";
    public static SharedPreferences data;

    String contactos = "";
    String ids = "";

    private static Context context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView textnum,textmarca,textmodelo,textdesc;
        ImageView imgnew;
        ConstraintLayout cons;

        public DataObjectHolder(View itemView) {
            super(itemView);
            cons = (ConstraintLayout) itemView.findViewById(R.id.consrow);
            textnum = (TextView) itemView.findViewById(R.id.textrennum);
            textmarca = (TextView) itemView.findViewById(R.id.textrenmarca);
            textmodelo = (TextView) itemView.findViewById(R.id.textrenmodelo);
            imgnew = (ImageView) itemView.findViewById(R.id.imageViewnew);
            textdesc = (TextView) itemView.findViewById(R.id.textViewdescripcion);
            itemView.setOnClickListener(this);
            context = itemView.getContext();
        }

        @Override
        public void onClick(View v) {



        }
    }

    public void setOnItemClickListener(RecyclerviewAdapterResguardoDetalle.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public RecyclerviewAdapterResguardoDetalle(List<ArticuloResguardo> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public RecyclerviewAdapterResguardoDetalle.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_inv_fisico, parent, false);

        RecyclerviewAdapterResguardoDetalle.DataObjectHolder dataObjectHolder = new RecyclerviewAdapterResguardoDetalle.DataObjectHolder(view);

        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerviewAdapterResguardoDetalle.DataObjectHolder holder, final int position) {

        holder.textnum.setText(mDataset.get(position).getNoinv());
        holder.textmarca.setText(mDataset.get(position).getMarca());
        holder.textmodelo.setText(mDataset.get(position).getModelo());
        holder.textdesc.setText(mDataset.get(position).getCaracteristicas());
        holder.imgnew.setVisibility(View.INVISIBLE);

        holder.cons.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                int iddetalle = mDataset.get(position).getId();
                new AlertDialog.Builder(context)
                        .setTitle("Eliminar articulo")
                        .setMessage("¿Deseas eliminar este articulo del resguardo?")
                        .setPositiveButton("Aceptar",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();

                            }
                        })
                        .setNegativeButton("Cancelar",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                            }
                        })
                        .show();

                return true;
            }
        });

    }

    public void addItem(ArticuloResguardo dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }

}