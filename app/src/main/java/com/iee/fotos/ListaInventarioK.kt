package com.iee.fotos

import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import com.iee.fotos.inventario.Bienresg
import com.iee.fotos.inventario.Fisico
import com.kroegerama.kaiteki.bcode.BarcodeResultListener
import com.kroegerama.kaiteki.bcode.ui.showBarcodeAlertDialog
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback

class ListaInventarioK : AppCompatActivity(), BarcodeResultListener {

    lateinit var recyclerview: RecyclerView
    lateinit var textnombre: TextView
    lateinit var madapter: RecyclerviewAdapterInventario
    lateinit var mLayoutmanager: RecyclerView.LayoutManager
    lateinit var customprogressbar: CustomProgressBar
    lateinit var noemp: String
    lateinit var apiInterface: APIInterface
    lateinit var fabbarcocde: FloatingActionButton
    lateinit var listainv: List<Bienresg>
    var noinvgl: Int = 0
    var numinv = ""
    var numinvnuevo = ""
    var iscap = 0
    var statusstring = ""
    var EdificioID = 0
    lateinit var datos: SharedPreferences
    var filename = "datos"
    var islogin = ""
    var iduser = 0
    var NombreEmpleado = ""

    override fun onBarcodeScanCancelled() {

    }

    override fun onBarcodeResult(result: Result): Boolean {

        doAsync {
            codigobarra(result.toString())

            fabbarcocde.post {
                if(noinvgl == 0)
                {
                    var  dialogBuilder = AlertDialog.Builder(this@ListaInventarioK)
                    var layoutView = layoutInflater.inflate(R.layout.dialog_warning, null)
                    var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                    dialogBuilder.setView(layoutView)
                    var   alertDialog = dialogBuilder.create()
                    alertDialog.show()
                    dialogButton.setOnClickListener {
                        alertDialog.dismiss()

                    }
                }
                else
                {
                    //var arrelgo = listainv.size
                        doAsync{
                            setinventario()
                        }
                }
            }
        }



        return true
    }

    companion object {
        private const val TAG = "FragDialogExamples"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_bienes)

        noemp = intent.getStringExtra("noemp").toString()
        EdificioID = intent.getIntExtra("EdificioID",0)
        NombreEmpleado = intent.getStringExtra("NombreEmpleado").toString()
        mLayoutmanager = LinearLayoutManager(this)
        datos = getSharedPreferences(filename, 0)
        islogin = datos.getString("iduser","").toString()
        iduser = islogin.toInt()

        apiInterface = APIClient.getClient().create(APIInterface::class.java)
        recyclerview = findViewById(R.id.recyclerbienes)
        textnombre = findViewById(R.id.textViewEmpleado)
        fabbarcocde = findViewById(R.id.floatingActionButton2)
        fabbarcocde.setImageDrawable(getDrawable(R.drawable.ic_barcode))

        textnombre.text = NombreEmpleado

        fabbarcocde.setOnClickListener {
            showBarcodeAlertDialog(
                    owner = this,
                    listener = this,
                    formats = listOf(BarcodeFormat.CODE_128),
                    barcodeInverted = false
            )
        }

        customprogressbar = CustomProgressBar()
        customprogressbar.show(this,"Cargando...")
        val call = apiInterface.doGetFisicoLista(noemp)
        doAsync{
            call.enqueue(object : retrofit2.Callback<Fisico>{
                override fun onFailure(call: Call<Fisico>?, t: Throwable?) {
                    customprogressbar.dialog.dismiss()
                }

                override fun onResponse(call: Call<Fisico>?, response: Response<Fisico>?) {
                    val result = response!!.body()
                    listainv = result!!.bienresgs
                    recyclerview.setHasFixedSize(true)
                    madapter = RecyclerviewAdapterInventario(listainv)
                    recyclerview.post(){
                        recyclerview.layoutManager = mLayoutmanager
                        recyclerview.adapter = madapter
                        customprogressbar.dialog.dismiss()
                }
            }

            })
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_recibido, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_Recibir) {
            val view = Intent(this@ListaInventarioK, ListaInventarioEnviadosK::class.java)
            view.putExtra("noemp",noemp)
            view.putExtra("EdificioID",EdificioID)
            startActivity(view)
        }
        else
        {

        }
        return super.onOptionsItemSelected(item)
    }

    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        init {
            execute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
        }
    }

    fun codigobarra(noinv: String)
    {
        val call = apiInterface.doGetBienNuevoDetail(noinv)
        val result = call.execute().body()
        noinvgl = result!!.idbien
        if (noinvgl == 0)
        {

        }
        else
        {
            numinv = result!!.numinv
            numinvnuevo = result!!.numinvNuevo
            iscap = result!!.capitalizable
        }


    }

    fun setinventario()
    {
        val call = apiInterface.doSetInventarioF(noemp,noinvgl,0,1,numinv,iscap,numinvnuevo,EdificioID,iduser)
        val result = call.execute().body()
        statusstring = result?.status.toString()
        if(statusstring.equals("ok")) {
            fabbarcocde.post {
                var dialogBuilder = AlertDialog.Builder(this)
                var layoutView = layoutInflater.inflate(R.layout.dialog_success, null)
                var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                dialogBuilder.setView(layoutView)
                var alertDialog = dialogBuilder.create()
                alertDialog.show()
                dialogButton.setOnClickListener {
                    alertDialog.dismiss()
                    customprogressbar = CustomProgressBar()
                    customprogressbar.show(this, "Cargando...")
                    val call = apiInterface.doGetFisicoLista(noemp)
                    doAsync {
                        call.enqueue(object : retrofit2.Callback<Fisico> {
                            override fun onFailure(call: Call<Fisico>?, t: Throwable?) {
                                customprogressbar.dialog.dismiss()
                            }

                            override fun onResponse(call: Call<Fisico>?, response: Response<Fisico>?) {
                                val result = response!!.body()
                                listainv = result!!.bienresgs
                                recyclerview.setHasFixedSize(true)
                                madapter = RecyclerviewAdapterInventario(listainv)
                                recyclerview.post() {
                                    recyclerview.layoutManager = mLayoutmanager
                                    recyclerview.adapter = madapter
                                    customprogressbar.dialog.dismiss()
                                }
                            }

                        })
                    }
                }
            }
        }
        else if (statusstring.equals("existe"))
        {
            fabbarcocde.post {
                var dialogBuilder = AlertDialog.Builder(this)
                var layoutView = layoutInflater.inflate(R.layout.activity_dialog_repetido, null)
                var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                dialogBuilder.setView(layoutView)
                var alertDialog = dialogBuilder.create()
                alertDialog.show()
                dialogButton.setOnClickListener {
                    alertDialog.dismiss()
                }
            }
        }
        else if (statusstring.equals("Resguardado"))
        {
            fabbarcocde.post {
                var dialogBuilder = AlertDialog.Builder(this)
                var layoutView = layoutInflater.inflate(R.layout.activity_dialog_repetido, null)
                var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                var dialogtitle = layoutView.findViewById<TextView>(R.id.txt_msg)
                var dialogsubtitle = layoutView.findViewById<TextView>(R.id.textView2)
                dialogtitle.setText("Artitulo resguardado")
                dialogsubtitle.setText("Este articulo se encuentra actualmente resguardado")
                dialogBuilder.setView(layoutView)
                var alertDialog = dialogBuilder.create()
                alertDialog.show()
                dialogButton.setOnClickListener {
                    alertDialog.dismiss()
                }
            }
        }
        else if (statusstring.equals("Novigente"))
        {
            fabbarcocde.post {
                var dialogBuilder = AlertDialog.Builder(this)
                var layoutView = layoutInflater.inflate(R.layout.activity_dialog_repetido, null)
                var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                var dialogtitle = layoutView.findViewById<TextView>(R.id.txt_msg)
                var dialogsubtitle = layoutView.findViewById<TextView>(R.id.textView2)
                dialogtitle.setText("Artitulo no vigente")
                dialogsubtitle.setText("Este articulo no se cencuentra vigente en este momento")
                dialogBuilder.setView(layoutView)
                var alertDialog = dialogBuilder.create()
                alertDialog.show()
                dialogButton.setOnClickListener {
                    alertDialog.dismiss()
                }
            }
        }

    }

    fun metodoborradoincepcion(idresg: Int)
    {
        doAsync{
            BorrarInventario(idresg)
        }
    }

    fun BorrarInventario(idresg: Int)
    {

        val call = apiInterface.doDeleteInventario(idresg)
        val result = call.execute().body()
        statusstring = result?.status.toString()
        if(statusstring.equals("ok")) {

            runOnUiThread {
                AlertDialog.Builder(this)
                        .setTitle("Eliminar Bien")
                        .setMessage("haz eliminado el bien correctamente")
                        .setNeutralButton("Continuar")  { dialog, whichButton ->
                            dialog.dismiss()
                            customprogressbar = CustomProgressBar()
                            customprogressbar.show(this, "Cargando...")
                            val call = apiInterface.doGetFisicoLista(noemp)
                            doAsync {
                                call.enqueue(object : retrofit2.Callback<Fisico> {
                                    override fun onFailure(call: Call<Fisico>?, t: Throwable?) {
                                        customprogressbar.dialog.dismiss()
                                    }

                                    override fun onResponse(call: Call<Fisico>?, response: Response<Fisico>?) {
                                        val result = response!!.body()
                                        listainv = result!!.bienresgs
                                        recyclerview.setHasFixedSize(true)
                                        madapter = RecyclerviewAdapterInventario(listainv)
                                        recyclerview.post() {
                                            recyclerview.layoutManager = mLayoutmanager
                                            recyclerview.adapter = madapter
                                            customprogressbar.dialog.dismiss()
                                        }
                                    }

                                })
                            }

                        }
                        .show()
            }
        }
    }

}