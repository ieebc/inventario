package com.iee.fotos;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.iee.fotos.inventario.Bienresg;
import com.iee.fotos.inventario.Fisico;
import com.iee.fotos.inventario.Bienesm;
import com.iee.fotos.inventario.Fisico;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListaInventario extends AppCompatActivity {

    RecyclerView recyclerView;
    APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
    private  RecyclerviewAdapterInventario mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    CustomProgressBar customProgressBar;
    String noemp = "";
    int iscap = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_bienes);

        customProgressBar = new CustomProgressBar();
        noemp = getIntent().getStringExtra("noemp");
        iscap = getIntent().getIntExtra("iscap",0);

        FloatingActionButton agregar = (FloatingActionButton) findViewById(R.id.floatingActionButton2);
        agregar.setImageDrawable(getDrawable(R.drawable.ic_barcode));
        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recyclerbienes);
        customProgressBar.show(this,"Cargando...");
        Call<Fisico> call1 = apiInterface.doGetFisicoLista(noemp);
        call1.enqueue(new Callback<Fisico>() {
            @Override
            public void onResponse(Call<Fisico> call, Response<Fisico> response) {
                Fisico Fisico = response.body();
                List<Bienresg> listabienes = Fisico.getBienresgs();

                recyclerView.setHasFixedSize(true);
                mLayoutManager = new LinearLayoutManager(ListaInventario.this);
                recyclerView.setLayoutManager(mLayoutManager);
                mAdapter = new RecyclerviewAdapterInventario(listabienes);
                recyclerView.setAdapter(mAdapter);
                runLayoutAnimation(recyclerView);
                customProgressBar.dialog.dismiss();

            }

            @Override
            public void onFailure(Call<Fisico> call, Throwable t) {
                customProgressBar.dialog.dismiss();
            }
        });

    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        super.onBackPressed();  // optional depending on your needs
    }



    private void runLayoutAnimation(final RecyclerView recyclerView) {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_from_right);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }

}
