package com.iee.fotos

import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import com.iee.fotos.inventario.BienesTransito
import com.iee.fotos.inventario.BienesTransito__1
import com.iee.fotos.inventario.Fisico
import com.kroegerama.kaiteki.bcode.BarcodeResultListener
import com.kroegerama.kaiteki.bcode.ui.showBarcodeAlertDialog
import kotlinx.android.synthetic.main.activity_lista_bienes.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class EnviarBienesActivity : AppCompatActivity(), BarcodeResultListener {

    lateinit var apiInterface: APIInterface
    lateinit var nombrerecibetxt: TextView
    lateinit var nombreenviatxt: TextView
    lateinit var nombreEdificiotxt: TextView
    lateinit var RecyclerTransito: RecyclerView
    lateinit var mLayoutmanager: RecyclerView.LayoutManager
    lateinit var madapter: RecyclerviewAdapterTransito
    lateinit var Scan: FloatingActionButton
    lateinit var customprogressbar: CustomProgressBar
    lateinit var datos: SharedPreferences
    lateinit var listainv: List<BienesTransito__1>
    var filename = "datos"
    var nombreEnvia = ""
    var nombreRecibe = ""
    var nombreEdifico = ""
    var idEdificio = 0
    var iduser = 0

    override fun onBarcodeScanCancelled() {

    }

    override fun onBarcodeResult(result: Result): Boolean {

        PreguntaObservaciones(result.toString())

        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibeyenvia)

        datos = getSharedPreferences(filename, 0)
        val iduserstring = datos.getString("iduser", "").toString()
        iduser = Integer.valueOf(iduserstring)
        mLayoutmanager = LinearLayoutManager(this)
        apiInterface = APIClient.getClient().create(APIInterface::class.java)
        nombreEdificiotxt = findViewById<TextView>(R.id.textViewEdificio)
        nombreenviatxt = findViewById<TextView>(R.id.textViewEnvia)
        nombrerecibetxt = findViewById<TextView>(R.id.textViewRecibe)
        Scan = findViewById<FloatingActionButton>(R.id.floatingActionButtonScan)
        RecyclerTransito = findViewById<RecyclerView>(R.id.RecyclerViewTransito)

        nombreEnvia = intent.getStringExtra("nombreEnvia").toString()
        nombreRecibe = intent.getStringExtra("nombreRecibe").toString()
        nombreEdifico = intent.getStringExtra("nombreEdificio").toString()
        idEdificio = intent.getIntExtra("EdificioID", 0)

        nombreEdificiotxt.text = "Edificio que Envia: $nombreEdifico"
        nombreenviatxt.text = "Nombre que envia: $nombreEnvia"
        nombrerecibetxt.text = "Nombre que recibe: $nombreRecibe"

        Scan.setOnClickListener {
            showBarcodeAlertDialog(
                    owner = this,
                    listener = this,
                    formats = listOf(BarcodeFormat.CODE_128),
                    barcodeInverted = false
            )
        }

    }

    override fun onStart() {
        super.onStart()
        llenarRecyclerview()
    }

    override fun onRestart() {
        super.onRestart()
    }

    fun llenarRecyclerview(){
        customprogressbar = CustomProgressBar()
        customprogressbar.show(this, "Cargando...")
        val call = apiInterface.doGetTransito(idEdificio)
        doAsync{
            call.enqueue(object : retrofit2.Callback<BienesTransito> {
                override fun onFailure(call: Call<BienesTransito>?, t: Throwable?) {
                    customprogressbar.dialog.dismiss()
                }

                override fun onResponse(call: Call<BienesTransito>?, response: Response<BienesTransito>?) {
                    val result = response!!.body()
                    listainv = result!!.bienesTransito
                    RecyclerTransito.setHasFixedSize(true)
                    madapter = RecyclerviewAdapterTransito(listainv)
                    RecyclerTransito.post() {
                        RecyclerTransito.layoutManager = mLayoutmanager
                        RecyclerTransito.adapter = madapter
                        customprogressbar.dialog.dismiss()
                    }
                }

            })
        }
    }

    fun PreguntaObservaciones(codigo: String){
        //crear edittext
        val input = EditText(this)
        val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        input.layoutParams = lp

        AlertDialog.Builder(this)
                .setTitle("Observaciones")
                .setMessage("¿Este Articulo tiene observaciones?")
                .setView(input)
                .setPositiveButton("Si") { dialog, whichButton ->
                    dialog.dismiss()
                    EnviarInventario(codigo,input.text.toString())
                }
                .setNegativeButton("No") { dialog, whichButton ->
                    dialog.dismiss()
                    EnviarInventario(codigo, "")
                }
                .show()
    }

    fun EnviarInventario(codigo: String, observaciones: String){
        val calendar: Calendar = Calendar.getInstance()
        val hour12hrs = calendar[Calendar.HOUR].toString()
        val minutes = calendar[Calendar.MINUTE].toString()
        val hora = "$hour12hrs:$minutes"
        val call = apiInterface.doGetAddTransito(idEdificio, iduser, nombreEnvia, nombreRecibe, codigo, hora, observaciones)
        val result = call.execute().body()
        val statusstring = result?.status.toString()
        if(statusstring.equals("ok")) {
            Scan.post {
                var dialogBuilder = AlertDialog.Builder(this)
                var layoutView = layoutInflater.inflate(R.layout.dialog_success, null)
                var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                dialogBuilder.setView(layoutView)
                var alertDialog = dialogBuilder.create()
                alertDialog.show()
                dialogButton.setOnClickListener {
                    alertDialog.dismiss()
                    llenarRecyclerview()
                }
            }
        }
        else if (statusstring.equals("NoExiste"))
        {
            Scan.post {
                var dialogBuilder = AlertDialog.Builder(this)
                var layoutView = layoutInflater.inflate(R.layout.activity_dialog_noexiste, null)
                var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                dialogBuilder.setView(layoutView)
                var alertDialog = dialogBuilder.create()
                alertDialog.show()
                dialogButton.setOnClickListener {
                    alertDialog.dismiss()
                    val view = Intent(this, AltaTransitoActivity::class.java)
                    view.putExtra("nombreRecibe",nombreRecibe)
                    view.putExtra("nombreEnvia",nombreEnvia)
                    view.putExtra("EdificioID",idEdificio)
                    startActivity(view)
                }
            }
        }
        else if (statusstring.equals("EnTransito"))
        {
            Scan.post {
                var dialogBuilder = AlertDialog.Builder(this)
                var layoutView = layoutInflater.inflate(R.layout.activity_dialog_transito, null)
                var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                dialogBuilder.setView(layoutView)
                var alertDialog = dialogBuilder.create()
                alertDialog.show()
                dialogButton.setOnClickListener {
                    alertDialog.dismiss()
                }
            }
        }
    }

    fun BorrarArticulo(idarticulo: Int){

        doAsync{
            val call = apiInterface.doSetDeleteTransito(idarticulo)
            val result = call.execute().body()
            val statusstring = result?.status.toString()

            if (statusstring.equals("ok")) {
                runOnUiThread {
                    AlertDialog.Builder(this)
                            .setTitle("Eliminar articulo")
                            .setMessage("haz eliminado el articulo correctamente")
                            .setNeutralButton("Continuar")  { dialog, whichButton ->
                                dialog.dismiss()
                                llenarRecyclerview()
                            }
                            .show()
                }
            }
        }

    }

    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        init {
            execute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
        }
    }


}