package com.iee.fotos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;


public class MainActivity extends AppCompatActivity {

    ImageButton camara,archivos,scanner;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        camara = (ImageButton) findViewById(R.id.imageButton2);
        archivos = (ImageButton) findViewById(R.id.imageButton3);
        scanner = (ImageButton) findViewById(R.id.imageButton);

        camara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent view = new Intent(MainActivity.this,CameraActivity.class);
                startActivity(view);

            }
        });

        archivos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent view = new Intent(MainActivity.this,ScannerK.class);
                startActivity(view);
            }
        });

    }


}
