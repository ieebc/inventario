package com.iee.fotos

import android.Manifest
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import com.kroegerama.kaiteki.bcode.BarcodeResultListener
import com.kroegerama.kaiteki.bcode.ui.showBarcodeAlertDialog
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions

class MenuActivity : AppCompatActivity(), BarcodeResultListener {

    lateinit var BuscarNumero: CustomNumero
    var islogin = ""
    lateinit var datos: SharedPreferences
    var filename = "datos"

    override fun onBarcodeScanCancelled() {

    }

    override fun onBarcodeResult(result: Result): Boolean {
        Log.d(TAG, "Result: $result")
        val intent = Intent(this,Detallebien::class.java)
        intent.putExtra("numbien",result.toString())
        startActivity(intent)
        return true
    }

    companion object {
        private const val TAG = "FragDialogExamples"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_principal)

        val permissions = arrayOf<String>(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        Permissions.check(
                this,
                permissions,
                null,
                null,
                object : PermissionHandler() {
                    override fun onGranted() {
                        // do your task.

                    }
                })


        var barcodebutton = findViewById<ConstraintLayout>(R.id.imageButton)
        barcodebutton.setOnClickListener {
            showBarcodeAlertDialog(
                    owner = this,
                    listener = this,
                    formats = listOf(BarcodeFormat.CODE_128),
                    barcodeInverted = false

            )
        }

        var bienbutton = findViewById<ConstraintLayout>(R.id.imageButton4)
        bienbutton.setOnClickListener {
            val intent = Intent(this,ListaBienesActivity::class.java)
            startActivity(intent)
        }

        var asignarbutton = findViewById<ConstraintLayout>(R.id.constraintLayout2)
        asignarbutton.setOnClickListener {
            val intent = Intent(this,IngresarUsuario::class.java)
            startActivity(intent)
        }

        var Manualbutton = findViewById<ConstraintLayout>(R.id.ImageButtonManual)
        Manualbutton.setOnClickListener {
            generardialogo()
        }

        var Enviarbutton = findViewById<ConstraintLayout>(R.id.ConstraintEnviar)
        Enviarbutton.setOnClickListener {
            val intent = Intent(this,ParametrosBienesActivity::class.java)
            intent.putExtra("Recibe",0)
            startActivity(intent)
        }

        var Recibirbutton = findViewById<ConstraintLayout>(R.id.ConstraintRecibir)
        Recibirbutton.setOnClickListener {
            val intent = Intent(this,ParametrosBienesActivity::class.java)
            intent.putExtra("Recibe",1)
            startActivity(intent)
        }

        var Monitorearbutton = findViewById<ConstraintLayout>(R.id.ConstraintMonitorear)
        Monitorearbutton.setOnClickListener {
            val intent = Intent(this,MonitoreoActivity::class.java)
            startActivity(intent)
        }

        var Resguardobutton = findViewById<ConstraintLayout>(R.id.ConstraintResguardo)
        Resguardobutton.setOnClickListener {
            val intent = Intent(this, ListaResguardoActivity::class.java)
            startActivity(intent)
        }




    }

    fun generardialogo() {
        var dialogBuilder = AlertDialog.Builder(this)
        var layoutView = layoutInflater.inflate(R.layout.dialog_escribir_codigo, null)
        var dialogButton = layoutView.findViewById(R.id.button) as Button
        var cancelButton = layoutView.findViewById<Button>(R.id.button2)
        var editnumero = layoutView.findViewById<EditText>(R.id.Edit_Numero)
        dialogBuilder.setView(layoutView)
        var alertDialog = dialogBuilder.create()
        alertDialog.show()
        dialogButton.setOnClickListener {
            val intent = Intent(this,Detallebien::class.java)
            intent.putExtra("numbien",editnumero.text.toString())
            startActivity(intent)
            //alertDialog.dismiss()
        }
        cancelButton.setOnClickListener {
            alertDialog.dismiss()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        val intent = Intent(this, ExitService::class.java)
        startService(intent)
    }

}