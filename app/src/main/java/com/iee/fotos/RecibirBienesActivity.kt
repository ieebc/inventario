package com.iee.fotos

import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import com.iee.fotos.inventario.BienesTransito
import com.iee.fotos.inventario.BienesTransito__1
import com.kroegerama.kaiteki.bcode.BarcodeResultListener
import com.kroegerama.kaiteki.bcode.ui.showBarcodeAlertDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RecibirBienesActivity : AppCompatActivity(), BarcodeResultListener {

    lateinit var apiInterface: APIInterface
    lateinit var nombrerecibetxt: TextView
    lateinit var nombreenviatxt: TextView
    lateinit var nombreEdificiotxt: TextView
    lateinit var RecyclerTransito: RecyclerView
    lateinit var mLayoutmanager: RecyclerView.LayoutManager
    lateinit var madapter: RecyclerviewAdapterTransito
    lateinit var Scan: FloatingActionButton
    lateinit var customprogressbar: CustomProgressBar
    lateinit var datos: SharedPreferences
    lateinit var listainv: List<BienesTransito__1>
    var filename = "datos"
    var nombreEnvia = ""
    var nombreRecibe = ""
    var nombreEdifico = ""
    var idEdificio = 0
    var iduser = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibeyenvia)

        datos = getSharedPreferences(filename, 0)
        val iduserstring = datos.getString("iduser", "").toString()
        iduser = Integer.valueOf(iduserstring)
        mLayoutmanager = LinearLayoutManager(this)
        apiInterface = APIClient.getClient().create(APIInterface::class.java)
        nombreEdificiotxt = findViewById<TextView>(R.id.textViewEdificio)
        nombreenviatxt = findViewById<TextView>(R.id.textViewEnvia)
        nombrerecibetxt = findViewById<TextView>(R.id.textViewRecibe)
        Scan = findViewById<FloatingActionButton>(R.id.floatingActionButtonScan)
        RecyclerTransito = findViewById<RecyclerView>(R.id.RecyclerViewTransito)

        nombreEnvia = intent.getStringExtra("nombreEnvia").toString()
        nombreRecibe = intent.getStringExtra("nombreRecibe").toString()
        nombreEdifico = intent.getStringExtra("nombreEdificio").toString()
        idEdificio = intent.getIntExtra("EdificioID", 0)

        nombreEdificiotxt.text = "Edificio que Recibe: $nombreEdifico"
        nombreenviatxt.text = "Nombre que envia: $nombreEnvia"
        nombrerecibetxt.text = "Nombre que recibe: $nombreRecibe"

        Scan.setOnClickListener {
            showBarcodeAlertDialog(
                    owner = this,
                    listener = this,
                    formats = listOf(BarcodeFormat.CODE_128),
                    barcodeInverted = false
            )
        }

    }

    override fun onStart() {
        super.onStart()
        llenarRecyclerview()
    }

    override fun onBarcodeScanCancelled() {

    }

    override fun onBarcodeResult(result: Result): Boolean {

       RecibirArticulo(result.toString())
        return true
    }

    fun llenarRecyclerview(){
        customprogressbar = CustomProgressBar()
        customprogressbar.show(this, "Cargando...")
        val call = apiInterface.doGetTransitoRecibido(idEdificio)
        doAsync {
            call.enqueue(object : Callback<BienesTransito> {
                override fun onFailure(call: Call<BienesTransito>?, t: Throwable?) {
                    customprogressbar.dialog.dismiss()
                }

                override fun onResponse(call: Call<BienesTransito>?, response: Response<BienesTransito>?) {
                    val result = response!!.body()
                    listainv = result!!.bienesTransito
                    RecyclerTransito.setHasFixedSize(true)
                    madapter = RecyclerviewAdapterTransito(listainv)
                    runOnUiThread {
                        RecyclerTransito.layoutManager = mLayoutmanager
                        RecyclerTransito.adapter = madapter
                        customprogressbar.dialog.dismiss()
                    }
                }

            })
        }
    }

    fun RecibirArticulo(noinv : String) {
        doAsync{
            val call = apiInterface.doSetTransitoRecibido(idEdificio,nombreRecibe,noinv)
            val result = call.execute().body()
            val statusstring = result?.status.toString()
            if(statusstring.equals("ok")) {
                runOnUiThread {
                    var dialogBuilder = AlertDialog.Builder(this)
                    var layoutView = layoutInflater.inflate(R.layout.dialog_success, null)
                    var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                    dialogBuilder.setView(layoutView)
                    var alertDialog = dialogBuilder.create()
                    alertDialog.show()
                    dialogButton.setOnClickListener {
                        alertDialog.dismiss()
                        llenarRecyclerview()
                    }
                }
            }
            else if (statusstring.equals("NoExiste"))
            {
                runOnUiThread {
                    var dialogBuilder = AlertDialog.Builder(this)
                    var layoutView = layoutInflater.inflate(R.layout.activity_dialog_noexiste, null)
                    var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                    dialogBuilder.setView(layoutView)
                    var alertDialog = dialogBuilder.create()
                    alertDialog.show()
                    dialogButton.setOnClickListener {
                        alertDialog.dismiss()
                    }
                }
            }
            else if (statusstring.equals("Entregado"))
            {
                runOnUiThread {
                    var dialogBuilder = AlertDialog.Builder(this)
                    var layoutView = layoutInflater.inflate(R.layout.activity_dialog_entregado, null)
                    var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                    dialogBuilder.setView(layoutView)
                    var alertDialog = dialogBuilder.create()
                    alertDialog.show()
                    dialogButton.setOnClickListener {
                        alertDialog.dismiss()
                    }
                }
            }
        }
    }

    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        init {
            execute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
        }
    }

}