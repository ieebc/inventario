package com.iee.fotos;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.iee.fotos.inventario.Bienresg;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


public class RecyclerviewAdapterInventarioRecibido extends RecyclerView
        .Adapter<RecyclerviewAdapterInventarioRecibido
        .DataObjectHolder>{
    private static String LOG_TAG = "RecyclerviewAdapterInventarioRecibido";
    private List<Bienresg> mDataset;
    private static RecyclerviewAdapterInventarioRecibido.MyClickListener myClickListener;

    public static String filename = "datos";
    public static SharedPreferences data;

    String contactos = "";
    String ids = "";

    private static Context context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView textnum,textmarca,textmodelo,textdesc;
        ImageView imgnew;
        CheckBox checkrecibido;
        ConstraintLayout cons;

        public DataObjectHolder(View itemView) {
            super(itemView);
            cons = (ConstraintLayout) itemView.findViewById(R.id.consrow);
            textnum = (TextView) itemView.findViewById(R.id.textrennum);
            textmarca = (TextView) itemView.findViewById(R.id.textrenmarca);
            textmodelo = (TextView) itemView.findViewById(R.id.textrenmodelo);
            checkrecibido = (CheckBox) itemView.findViewById(R.id.checkrecibido);
            textdesc = (TextView) itemView.findViewById(R.id.textViewdesc);

            itemView.setOnClickListener(this);
            context = itemView.getContext();
        }

        @Override
        public void onClick(View v) {

        }
    }

    public void setOnItemClickListener(RecyclerviewAdapterInventarioRecibido.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public RecyclerviewAdapterInventarioRecibido(List<Bienresg> myDataset) {
        mDataset = myDataset;
        setHasStableIds(true);
    }

    @Override
    public RecyclerviewAdapterInventarioRecibido.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                             int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_inv_fisico_recibido, parent, false);

        RecyclerviewAdapterInventarioRecibido.DataObjectHolder dataObjectHolder = new RecyclerviewAdapterInventarioRecibido.DataObjectHolder(view);

        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerviewAdapterInventarioRecibido.DataObjectHolder holder, final int position) {

        holder.textnum.setText(mDataset.get(position).getNuminvNuevo());
        holder.textmarca.setText(mDataset.get(position).getMarca());
        holder.textmodelo.setText(mDataset.get(position).getModelo());
        holder.textdesc.setText(mDataset.get(position).getCaracteristicas());

        int estado = 0;
        estado = mDataset.get(position).getEstado();

        if(estado == 2)
        {
            holder.checkrecibido.setChecked(true);
        }
        else
        {
            holder.checkrecibido.setChecked(false);
        }

        holder.checkrecibido.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int IdResguardoF = mDataset.get(position).getIdresguardo();
                ((ListaInventarioEnviadosK)context).ModificarEstadoInception(IdResguardoF);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position){
        return position;
    }


    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }

}