package com.iee.fotos;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.iee.fotos.inventario.BienesTransito__1;

import java.util.ArrayList;
import java.util.List;

public class RecyclerviewAdapterMonitoreo extends RecyclerView
        .Adapter<RecyclerviewAdapterMonitoreo
        .DataObjectHolder>{
    private static String LOG_TAG = "RecyclerviewAdapterMonitoreo";
    private List<BienesTransito__1> mDataset;
    private static RecyclerviewAdapterMonitoreo.MyClickListener myClickListener;

    public static String filename = "datos";
    public static SharedPreferences data;

    String contactos = "";
    String ids = "";

    private static Context context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView textnum,textmarca,textmodelo,textdesc;
        ImageView imgnew;
        ConstraintLayout cons;

        public DataObjectHolder(View itemView) {
            super(itemView);
            cons = (ConstraintLayout) itemView.findViewById(R.id.consrow);
            textnum = (TextView) itemView.findViewById(R.id.textrennum);
            textmarca = (TextView) itemView.findViewById(R.id.textrenmarca);
            textmodelo = (TextView) itemView.findViewById(R.id.textrenmodelo);
            imgnew = (ImageView) itemView.findViewById(R.id.imageViewnew);
            textdesc = (TextView) itemView.findViewById(R.id.textViewdescripcion);
            itemView.setOnClickListener(this);
            context = itemView.getContext();
        }

        @Override
        public void onClick(View v) {



        }
    }

    public void setOnItemClickListener(RecyclerviewAdapterMonitoreo.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public RecyclerviewAdapterMonitoreo(List<BienesTransito__1> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public RecyclerviewAdapterMonitoreo.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_inv_fisico, parent, false);

        RecyclerviewAdapterMonitoreo.DataObjectHolder dataObjectHolder = new RecyclerviewAdapterMonitoreo.DataObjectHolder(view);

        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerviewAdapterMonitoreo.DataObjectHolder holder, final int position) {

        holder.textnum.setText(mDataset.get(position).getNuminv());
        holder.textmarca.setText(mDataset.get(position).getMarca());
        holder.textmodelo.setText(mDataset.get(position).getModelo());
        holder.textdesc.setText(mDataset.get(position).getCaracteristicas());

        int nuevoint = 0;
        nuevoint = mDataset.get(position).getNuevo();

        if(nuevoint == 1)
        {
            holder.imgnew.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.imgnew.setVisibility(View.INVISIBLE);
        }

        if(position == 0)
        {
            String NomEnvia = mDataset.get(position).getNombreEntrega();
            String NomRecibe = mDataset.get(position).getNombreRecibe();
            ((MonitoreoActivity)context).ObtenerNombres(NomEnvia,NomRecibe);
        }

        holder.cons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        holder.cons.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                int idarticulo = mDataset.get(position).getTransitoID();
                new AlertDialog.Builder(context)
                        .setTitle("Eliminar articulo")
                        .setMessage("¿Deseas eliminar este articulo?")
                        .setPositiveButton("Aceptar",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                                ((EnviarBienesActivity)context).BorrarArticulo(idarticulo);
                            }
                        })
                        .setNegativeButton("Cancelar",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                            }
                        })
                        .show();

                return true;
            }
        });

    }

    public void addItem(BienesTransito__1 dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }

}