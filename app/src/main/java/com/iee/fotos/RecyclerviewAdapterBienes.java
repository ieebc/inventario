package com.iee.fotos;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.iee.fotos.inventario.Bienesm;

import java.util.ArrayList;
import java.util.List;

public class RecyclerviewAdapterBienes extends RecyclerView
        .Adapter<RecyclerviewAdapterBienes
        .DataObjectHolder> implements Filterable {
    private static String LOG_TAG = "RecyclerviewAdapterBienes";
    private List<Bienesm> mDataset;
    private List<Bienesm> mDatasetFiltered;
    private static RecyclerviewAdapterBienes.MyClickListener myClickListener;

    public static String filename = "datos";
    public static SharedPreferences data;

    String contactos = "";
    String ids = "";
    private static Context context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textnum,textmarca,textmodelo,textserie;
        ConstraintLayout cons;

        public DataObjectHolder(View itemView) {
            super(itemView);
            cons = (ConstraintLayout) itemView.findViewById(R.id.consrow);
            textnum = (TextView) itemView.findViewById(R.id.textrennum);
            textmarca = (TextView) itemView.findViewById(R.id.textrenmarca);
            textmodelo = (TextView) itemView.findViewById(R.id.textrenmodelo);
            textserie = (TextView) itemView.findViewById(R.id.textrenserie);
            itemView.setOnClickListener(this);
            context = itemView.getContext();
        }

        @Override
        public void onClick(View v) {

        }
    }

    public void setOnItemClickListener(RecyclerviewAdapterBienes.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public RecyclerviewAdapterBienes(List<Bienesm> myDataset) {
        mDataset = myDataset;
        mDatasetFiltered = myDataset;
    }

    @Override
    public RecyclerviewAdapterBienes.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_bien, parent, false);

        RecyclerviewAdapterBienes.DataObjectHolder dataObjectHolder = new RecyclerviewAdapterBienes.DataObjectHolder(view);

        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerviewAdapterBienes.DataObjectHolder holder, final int position) {

        //   String name = mDataset.get(position).getmText1();
        //   holder.check.setText(name);

        if(mDatasetFiltered.get(position).getCapitalizable() == 2) {
            holder.textnum.setText(mDatasetFiltered.get(position).getNuminv());
        }
        else
        {
            holder.textnum.setText(mDatasetFiltered.get(position).getNuminv());
        }

        holder.textmarca.setText(mDatasetFiltered.get(position).getMarca());
        holder.textmodelo.setText(mDatasetFiltered.get(position).getModelo());
        holder.textserie.setText(mDatasetFiltered.get(position).getSerie());

        holder.cons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String numerobien = "";
                if(mDatasetFiltered.get(position).getCapitalizable() == 2) {
                    numerobien = mDatasetFiltered.get(position).getNuminv();
                }
                else
                {
                    numerobien = mDatasetFiltered.get(position).getNuminv();
                }

                int capi = mDatasetFiltered.get(position).getCapitalizable();
                Intent view = new Intent(context,Detallebien.class);
                view.putExtra("numbien",numerobien);
                view.putExtra("capitalizable",capi);
                context.startActivity(view);

            }
        });

    }

    public void addItem(Bienesm dataObj, int index) {
        mDatasetFiltered.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDatasetFiltered.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDatasetFiltered.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mDatasetFiltered = mDataset;
                } else {
                    List<Bienesm> filteredList = new ArrayList<>();
                    for (Bienesm row : mDataset) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getNuminv().toLowerCase().contains(charString.toLowerCase()) || row.getNuminvNuevo().toLowerCase().contains(charString.toLowerCase()) || row.getMarca().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    mDatasetFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mDatasetFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mDatasetFiltered = (ArrayList<Bienesm>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

}
