package com.iee.fotos

import android.os.AsyncTask
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.iee.fotos.inventario.Bienresg
import com.iee.fotos.inventario.BienesList
import com.iee.fotos.inventario.Bienesm
import retrofit2.Call
import retrofit2.Response

class ListaBienesActivityK : AppCompatActivity() {

    lateinit var recyclerview: RecyclerView
    lateinit var madapter: RecyclerviewAdapterBienes
    lateinit var mLayoutmanager: RecyclerView.LayoutManager
    lateinit var customprogressbar: CustomProgressBar
    lateinit var noemp: String
    lateinit var apiInterface: APIInterface
    lateinit var fabbarcocde: FloatingActionButton
    lateinit var listainv: List<Bienesm>
    var iscap = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_bienes)

       // iscap = intent.getIntExtra("iscap",0)
        mLayoutmanager = LinearLayoutManager(this)

        apiInterface = APIClient.getClient().create(APIInterface::class.java)
        recyclerview = findViewById(R.id.recyclerbienes)

        customprogressbar = CustomProgressBar()
        customprogressbar.show(this,"Cargando...")
        val call = apiInterface.doGetBienLista();
        
        doAsync{
            call.enqueue(object : retrofit2.Callback<BienesList>{
                override fun onFailure(call: Call<BienesList>?, t: Throwable?) {
                    customprogressbar.dialog.dismiss()
                }

                override fun onResponse(call: Call<BienesList>?, response: Response<BienesList>?) {
                    val result = response!!.body()
                    listainv = result!!.bienesm
                    recyclerview.setHasFixedSize(true)
                    madapter = RecyclerviewAdapterBienes(listainv)
                    recyclerview.post(){
                        recyclerview.layoutManager = mLayoutmanager
                        recyclerview.adapter = madapter
                        customprogressbar.dialog.dismiss()
                    }
                }

            })
        }
        
        
    }

    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        init {
            execute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
        }
    }
    

}