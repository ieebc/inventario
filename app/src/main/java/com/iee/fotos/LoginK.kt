package com.iee.fotos

import android.Manifest
import android.app.AlertDialog
import android.app.Dialog

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import kotlinx.android.synthetic.main.dialog_progress_bar.view.*

class LoginK : AppCompatActivity() {

    lateinit var apiInterface: APIInterface
    lateinit var usertxt: TextView
    lateinit var passtxt: TextView
    lateinit var userstring: String
    lateinit var passstring: String
    lateinit var guardarbtn: Button
    lateinit var iduser: String
    lateinit var dialog: Dialog
    val progressBar = CustomProgressBar()
    lateinit var datos: SharedPreferences
    var filename = "datos"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        apiInterface = APIClient.getClient().create(APIInterface::class.java)
        usertxt = findViewById(R.id.usertxt) as EditText
        passtxt = findViewById(R.id.passtxt) as EditText
        guardarbtn = findViewById(R.id.logibtn)
        datos = getSharedPreferences(filename, 0)
//        var islogin = datos.getString("iduser","")

//        if (islogin == "")
//        {
//            // No hace nada
//        }
//        else
//        {
//            val intent = Intent(this,MenuActivity::class.java)
//            startActivity(intent)
//            finish()
//        }


        val permissions = arrayOf<String>(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        Permissions.check(
                this,
                permissions,
                null,
                null,
                object : PermissionHandler() {
                    override fun onGranted() {
                        // do your task.

                    }
                })


        guardarbtn.setOnClickListener {

            userstring = usertxt.text.toString()
            passstring = passtxt.text.toString()
            progressBar.show(this,"Cargando...")

            doAsync{
                iniciosesion(userstring,passstring)
                guardarbtn.post(){
                    if (iduser.equals("Error"))
                    {
                        progressBar.dialog.dismiss()
                        val alertDialog = AlertDialog.Builder(this)
                        alertDialog.setTitle("Usuario Incorrecto")
                        alertDialog.setMessage("Revize sus credenciales").setNeutralButton("Aceptar", DialogInterface.OnClickListener { dialog, which -> dialog.cancel() })
                        val alert = alertDialog.create()
                        alert.show()
                    }
                    else
                    {
                        progressBar.dialog.dismiss()
                        val intent = Intent(this,MenuActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                }
            }

        }
    }



    fun iniciosesion(user: String, pass: String)
    {

        val call = apiInterface.doGetStatusSesion(user,pass)
        val result = call.execute().body()
        iduser = result?.status.toString()
        val editor = datos.edit()
        editor.putString("iduser",iduser)
        editor.commit()

    }

    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        init {
            execute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
        }
    }
}