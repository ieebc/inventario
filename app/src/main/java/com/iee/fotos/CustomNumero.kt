package com.iee.fotos

import android.app.Dialog
import android.content.Context
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import android.widget.Button
import android.widget.EditText
import androidx.annotation.NonNull
import kotlinx.android.synthetic.main.dialog_progress_bar.view.*

class CustomNumero {

    lateinit var dialog: Dialog
    lateinit var buscar: Button
    lateinit var numerotxt: EditText

    fun show(context: Context): Dialog {
        return show(context, null)
    }


    fun show(context: Context, title:CharSequence?): Dialog {
        val inflator = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflator.inflate(R.layout.dialog_escribir_codigo, null)
        if (title != null) {
            view.cp_title.text = title
        }

        view.cp_bg_view.setBackgroundColor(Color.parseColor("#60000000"))
        //view.cp_cardview.setCardBackgroundColor(Color.parseColor("#70000000"))
       // view.cp_title.setTextColor(Color.WHITE) //Text Color

        dialog = Dialog(context, R.style.CustomProgressBarTheme)
        dialog.setContentView(view)
        dialog.show()


        return dialog
    }

    fun setColorFilter(@NonNull drawable: Drawable, color:Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            drawable.colorFilter = BlendModeColorFilter(color, BlendMode.SRC_ATOP)
        } else {
            @Suppress("DEPRECATION")
            drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
        }
    }


}