package com.iee.fotos;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.iee.fotos.inventario.BienesTransito__1;
import com.iee.fotos.inventario.Resguardo;

import java.util.List;

public class RecyclerviewAdapterResguardo extends RecyclerView
        .Adapter<RecyclerviewAdapterResguardo
        .DataObjectHolder>{
    private static String LOG_TAG = "RecyclerviewAdapterResguardo";
    private List<Resguardo> mDataset;
    private static RecyclerviewAdapterResguardo.MyClickListener myClickListener;

    public static String filename = "datos";
    public static SharedPreferences data;

    String contactos = "";
    String ids = "";

    private static Context context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView textnombre,textarea,textdepto,textStatus;
        ConstraintLayout cons;

        public DataObjectHolder(View itemView) {
            super(itemView);
            cons = (ConstraintLayout) itemView.findViewById(R.id.cons1);
            textnombre = (TextView) itemView.findViewById(R.id.textViewNombre);
            textarea = (TextView) itemView.findViewById(R.id.textViewArea);
            textdepto = (TextView) itemView.findViewById(R.id.textViewDepto);
            textStatus = (TextView) itemView.findViewById(R.id.textViewEstatus);
            itemView.setOnClickListener(this);
            context = itemView.getContext();
        }

        @Override
        public void onClick(View v) {



        }
    }

    public void setOnItemClickListener(RecyclerviewAdapterResguardo.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public RecyclerviewAdapterResguardo(List<Resguardo> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public RecyclerviewAdapterResguardo.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_resguardo, parent, false);

        RecyclerviewAdapterResguardo.DataObjectHolder dataObjectHolder = new RecyclerviewAdapterResguardo.DataObjectHolder(view);

        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerviewAdapterResguardo.DataObjectHolder holder, final int position) {

        holder.textnombre.setText("Nombre: " + mDataset.get(position).getNombreemp());
        holder.textdepto.setText("Departamento: " + mDataset.get(position).getDeptodesc());
        holder.textarea.setText("Area: " + mDataset.get(position).getAreadesc());

        String status = mDataset.get(position).getEstatusresg();

        if(status.equals("A"))
        {
            holder.textStatus.setText("Abierto");
        }
        else
        {
            holder.textStatus.setText("Cerrado");
        }

        holder.cons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equals("A"))
                {
                    int ResguardoID = mDataset.get(position).getResguardoID();
                    String nombre = mDataset.get(position).getNombreemp().toString();
                    String departamento = mDataset.get(position).getDeptodesc().toString();
                    String area = mDataset.get(position).getAreadesc().toString();

                    Intent view = new Intent(context,ListaDetalleResguardo.class);
                    view.putExtra("nombre",nombre);
                    view.putExtra("area", area);
                    view.putExtra("depto", departamento);
                    view.putExtra("ResguardoID", ResguardoID);
                    context.startActivity(view);
                }
                else
                {}
            }
        });

    }

    public void addItem(Resguardo dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }

}
