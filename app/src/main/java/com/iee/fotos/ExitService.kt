package com.iee.fotos

import android.app.Service
import android.content.Intent
import android.content.SharedPreferences

import android.os.IBinder
import androidx.annotation.Nullable


class ExitService : Service() {
    lateinit var datos: SharedPreferences
    var filename = "datos"
    @Nullable
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        println("onTaskRemoved called")
        super.onTaskRemoved(rootIntent)
        datos = getSharedPreferences(filename, 0)
        val editor = datos.edit()
        editor.putString("iduser","")
        editor.commit()
        this.stopSelf()
    }
}