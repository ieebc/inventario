package com.iee.fotos;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.iee.fotos.inventario.Bienresg;

import java.util.ArrayList;
import java.util.List;

public class RecyclerviewAdapterInventario extends RecyclerView
        .Adapter<RecyclerviewAdapterInventario
        .DataObjectHolder>{
    private static String LOG_TAG = "RecyclerviewAdapterInventario";
    private List<Bienresg> mDataset;
    private static RecyclerviewAdapterInventario.MyClickListener myClickListener;

    public static String filename = "datos";
    public static SharedPreferences data;

    String contactos = "";
    String ids = "";

    private static Context context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView textnum,textmarca,textmodelo,textdesc;
        ImageView imgnew;
        ConstraintLayout cons;

        public DataObjectHolder(View itemView) {
            super(itemView);
            cons = (ConstraintLayout) itemView.findViewById(R.id.consrow);
            textnum = (TextView) itemView.findViewById(R.id.textrennum);
            textmarca = (TextView) itemView.findViewById(R.id.textrenmarca);
            textmodelo = (TextView) itemView.findViewById(R.id.textrenmodelo);
            imgnew = (ImageView) itemView.findViewById(R.id.imageViewnew);
            textdesc = (TextView) itemView.findViewById(R.id.textViewdescripcion);
            itemView.setOnClickListener(this);
            context = itemView.getContext();
        }

        @Override
        public void onClick(View v) {



        }
    }

    public void setOnItemClickListener(RecyclerviewAdapterInventario.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public RecyclerviewAdapterInventario(List<Bienresg> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public RecyclerviewAdapterInventario.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                         int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_inv_fisico, parent, false);

        RecyclerviewAdapterInventario.DataObjectHolder dataObjectHolder = new RecyclerviewAdapterInventario.DataObjectHolder(view);

        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerviewAdapterInventario.DataObjectHolder holder, final int position) {

        //   String name = mDataset.get(position).getmText1();
        //   holder.check.setText(name);

        holder.textnum.setText(mDataset.get(position).getNuminvNuevo());
        holder.textmarca.setText(mDataset.get(position).getMarca());
        holder.textmodelo.setText(mDataset.get(position).getModelo());
        holder.textdesc.setText(mDataset.get(position).getCaracteristicas());

        int nuevoint = 0;
        nuevoint = mDataset.get(position).getIsNew();

        if(nuevoint == 1)
        {
            holder.imgnew.setVisibility(View.INVISIBLE);
        }
        else
        {
            holder.imgnew.setVisibility(View.INVISIBLE);
        }

        holder.cons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String numerobien = mDataset.get(position).getNuminvNuevo();
                Intent view = new Intent(context,Detallebien.class);
                view.putExtra("numbien",numerobien);
                context.startActivity(view);

            }
        });

        holder.cons.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                int idresg = mDataset.get(position).getIdresguardo();
                new AlertDialog.Builder(context)
                        .setTitle("Eliminar Bien")
                        .setMessage("¿Deseas Eliminar bien de la lista?")
                        .setPositiveButton("Aceptar",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                               dialog.dismiss();
                               ((ListaInventarioK)context).metodoborradoincepcion(idresg);
                            }
                        })
                        .setNegativeButton("Cancelar",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                            }
                        })
                        .show();

               return true;
            }
        });

    }

    public void addItem(Bienresg dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }

}
