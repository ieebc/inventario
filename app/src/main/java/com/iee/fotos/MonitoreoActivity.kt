package com.iee.fotos

import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.iee.fotos.inventario.BienesTransito
import com.iee.fotos.inventario.BienesTransito__1
import com.iee.fotos.inventario.Edificiom
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MonitoreoActivity : AppCompatActivity() {

    lateinit var apiInterface: APIInterface
    lateinit var nombreenviatxt: EditText
    lateinit var continuarbtn: Button
    lateinit var spinnerEdificio: Spinner
    lateinit var RecyclerMonitoreo: RecyclerView
    lateinit var mLayoutmanager: RecyclerView.LayoutManager
    lateinit var madapter: RecyclerviewAdapterMonitoreo
    lateinit var datos: SharedPreferences
    lateinit var customprogressbar: CustomProgressBar
    lateinit var listainv: List<BienesTransito__1>
    var filename = "datos"
    lateinit var nombrearray: Array<String?>
    var adapter: ArrayAdapter<String>? = null
    lateinit var idarray: IntArray
    lateinit var listEdificio: List<Edificiom>
    var nombreEnvia = ""
    var idEdificio = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monitoreo)

        apiInterface = APIClient.getClient().create(APIInterface::class.java)
        spinnerEdificio = findViewById<Spinner>(R.id.spinnerEdificio)
        nombreenviatxt = findViewById<EditText>(R.id.NomEntrega)
        continuarbtn = findViewById<Button>(R.id.buttoncontinuar2)
        RecyclerMonitoreo = findViewById<RecyclerView>(R.id.RecyclerViewMonitoreo)
        mLayoutmanager = LinearLayoutManager(this)

        continuarbtn.setOnClickListener {
            idEdificio = idarray[spinnerEdificio.selectedItemPosition]
            nombreenviatxt.setText("")
            llenarRecyclerview(idEdificio)
        }

    }

    override fun onStart() {
        super.onStart()
        ObtenerEdificios()
    }

    fun ObtenerEdificios(){
        doAsync{
            val call = apiInterface.doGetEdificioLista()
            val result = call.execute().body()
            listEdificio = result!!.edificiom

            nombrearray = arrayOfNulls(listEdificio.size)
            idarray = IntArray(nombrearray.size)
            var indice = nombrearray.size - 1

            for (i in 0..indice) {
                nombrearray[i] = listEdificio[i].nombre.toString()
                idarray[i] = listEdificio[i].idedificio
            }
            runOnUiThread {
                adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, nombrearray)
                adapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinnerEdificio.adapter = adapter
            }
        }
    }

    fun llenarRecyclerview(EdificioID : Int){
        customprogressbar = CustomProgressBar()
        customprogressbar.show(this, "Cargando...")
        val call = apiInterface.doGetTransito(EdificioID)
        doAsync {
            call.enqueue(object : Callback<BienesTransito> {
                override fun onFailure(call: Call<BienesTransito>?, t: Throwable?) {
                    customprogressbar.dialog.dismiss()
                }

                override fun onResponse(call: Call<BienesTransito>?, response: Response<BienesTransito>?) {
                    val result = response!!.body()
                    listainv = result!!.bienesTransito
                    RecyclerMonitoreo.setHasFixedSize(true)
                    madapter = RecyclerviewAdapterMonitoreo(listainv)
                    runOnUiThread {
                        RecyclerMonitoreo.layoutManager = mLayoutmanager
                        RecyclerMonitoreo.adapter = madapter
                        customprogressbar.dialog.dismiss()
                    }
                }

            })
        }
    }

    fun ObtenerNombres(nomEnvia: String,nomRecibe: String){

            nombreenviatxt.setText(nomEnvia)

    }

    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        init {
            execute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
        }
    }
}