package com.iee.fotos

import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.iee.fotos.inventario.Edificiom

class ParametrosBienesActivity : AppCompatActivity() {

    lateinit var apiInterface: APIInterface
    lateinit var nomrecibetxt: EditText
    lateinit var nombreenviatxt: EditText
    lateinit var continuarbtn: Button
    lateinit var spinnerEdificio: Spinner
    val progressBar = CustomProgressBar()
    lateinit var datos: SharedPreferences
    var filename = "datos"
    lateinit var nombrearray: Array<String?>
    var adapter: ArrayAdapter<String>? = null
    lateinit var idarray: IntArray
    lateinit var listEdificio: List<Edificiom>
    var nombreRecibe = ""
    var nombreEnvia = ""
    var idEdificio = 0
    var nombreEdificio = ""
    var Recibe = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enviar_bienes_parametros)

        apiInterface = APIClient.getClient().create(APIInterface::class.java)
        nomrecibetxt = findViewById<EditText>(R.id.NomRecibe)
        nombreenviatxt = findViewById<EditText>(R.id.NomEntrega)
        continuarbtn = findViewById<Button>(R.id.buttoncontinuar)
        spinnerEdificio = findViewById<Spinner>(R.id.spinnerEdificio)
        Recibe = intent.getIntExtra("Recibe",0)

        if(Recibe == 1) {
            nombreenviatxt.visibility = View.INVISIBLE
        }

        doAsync{
            ObtenerEdificios()
            spinnerEdificio.post{
                adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, nombrearray)
                adapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinnerEdificio.adapter = adapter
            }
        }

        continuarbtn.setOnClickListener {
            nombreRecibe = nomrecibetxt.text.toString()
            nombreEnvia = nombreenviatxt.text.toString()
            idEdificio = idarray[spinnerEdificio.selectedItemPosition]
            nombreEdificio = nombrearray[spinnerEdificio.selectedItemPosition].toString()
            //ENVIAR A NUEVA PANTALLA
            if(Recibe == 1) {
                val view = Intent(this, RecibirBienesActivity::class.java)
                view.putExtra("nombreRecibe",nombreRecibe)
                view.putExtra("nombreEnvia",nombreEnvia)
                view.putExtra("nombreEdificio",nombreEdificio)
                view.putExtra("EdificioID",idEdificio)
                startActivity(view)
            }
            else
            {
                val view = Intent(this, EnviarBienesActivity::class.java)
                view.putExtra("nombreRecibe",nombreRecibe)
                view.putExtra("nombreEnvia",nombreEnvia)
                view.putExtra("nombreEdificio",nombreEdificio)
                view.putExtra("EdificioID",idEdificio)
                startActivity(view)
            }
        }
    }

    fun ObtenerEdificios(){
        val call = apiInterface.doGetEdificioLista()
        val result = call.execute().body()
        listEdificio = result!!.edificiom

        nombrearray = arrayOfNulls(listEdificio.size)
        idarray = IntArray(nombrearray.size)
        var indice = nombrearray.size - 1

        for (i in 0..indice) {
            nombrearray[i] = listEdificio[i].nombre.toString()
            idarray[i] = listEdificio[i].idedificio
        }
    }


    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        init {
            execute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
        }
    }


}

