package com.iee.fotos;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import androidx.appcompat.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.iee.fotos.inventario.BienesList;
import com.iee.fotos.inventario.Bienesm;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListaBienesActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
    private  RecyclerviewAdapterBienes mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    CustomProgressBar customProgressBar;
    private SearchView searchView;
    int iscap = 2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_bienes);

        customProgressBar = new CustomProgressBar();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerbienes);
        obtenerlista(1);

        FloatingActionButton agregar = (FloatingActionButton) findViewById(R.id.floatingActionButton2);
        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(iscap == 1)
                {
                    obtenerlista(iscap);
                    iscap = 2;
                }
                else
                {
                    obtenerlista(iscap);
                    iscap = 1;
                }
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                mAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }

    private void runLayoutAnimation(final RecyclerView recyclerView) {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_from_right);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }

    public void obtenerlista(int capitalizable)
    {

        if(capitalizable == 1)
        {
            setTitle("Inventario - Capitalizables");
            customProgressBar.show(this,"Cargando...");
            Call<BienesList> call1 = apiInterface.doGetBienLista();
            call1.enqueue(new Callback<BienesList>() {
                @Override
                public void onResponse(Call<BienesList> call, Response<BienesList> response) {
                    BienesList bienesList = response.body();
                    List<Bienesm> listabienes = bienesList.getBienesm();

                    recyclerView.setHasFixedSize(true);
                    mLayoutManager = new LinearLayoutManager(ListaBienesActivity.this);
                    recyclerView.setLayoutManager(mLayoutManager);
                    mAdapter = new RecyclerviewAdapterBienes(listabienes);
                    recyclerView.setAdapter(mAdapter);
                    runLayoutAnimation(recyclerView);
                    customProgressBar.dialog.dismiss();

                }

                @Override
                public void onFailure(Call<BienesList> call, Throwable t) {
                    customProgressBar.dialog.dismiss();
                }
            });
        }
        else
        {
            setTitle("Inventario - No Capitalizables");
            customProgressBar.show(this,"Cargando...");
            Call<BienesList> call1 = apiInterface.doGetBienListaCAP();
            call1.enqueue(new Callback<BienesList>() {
                @Override
                public void onResponse(Call<BienesList> call, Response<BienesList> response) {
                    BienesList bienesList = response.body();
                    List<Bienesm> listabienes = bienesList.getBienesm();

                    recyclerView.setHasFixedSize(true);
                    mLayoutManager = new LinearLayoutManager(ListaBienesActivity.this);
                    recyclerView.setLayoutManager(mLayoutManager);
                    mAdapter = new RecyclerviewAdapterBienes(listabienes);
                    recyclerView.setAdapter(mAdapter);
                    runLayoutAnimation(recyclerView);
                    customProgressBar.dialog.dismiss();

                }

                @Override
                public void onFailure(Call<BienesList> call, Throwable t) {
                    customProgressBar.dialog.dismiss();
                }
            });
        }

    }

}
