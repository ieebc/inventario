package com.iee.fotos;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.iee.fotos.inventario.Bienes;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Detallebien extends AppCompatActivity {

    APIInterface apiInterface;
    int idbien,EdificioID,TipoActivo;
    String marca,caracter,fechaalta,serie,modelo,numinvViejo,numInvNuevo,nofactura,observaciones;
    int capitalizable;
    CustomProgressBar customProgressBar;
    EditText numinvedt,numinvnuevoedt,marcaedt,modeloedt,serieedt,fechaaltaedt,facturaedt,caracteredt,observacionesedt;
    CheckBox capitalizalecheck;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_bien_nuevo);

        customProgressBar = new CustomProgressBar();
        String numinv = getIntent().getStringExtra("numbien");
        capitalizable = getIntent().getIntExtra("capitalizable",0);

        numinvedt = (EditText) findViewById(R.id.edtnuminv);
        numinvnuevoedt = (EditText) findViewById(R.id.edtnuminvnuevo);
        marcaedt = (EditText) findViewById(R.id.edtmarca);
        modeloedt = (EditText) findViewById(R.id.edtmodelo);
        serieedt = (EditText) findViewById(R.id.edtserie);
        fechaaltaedt = (EditText) findViewById(R.id.edtfechaalta);
        facturaedt = (EditText) findViewById(R.id.edtfactura);
        caracteredt = (EditText) findViewById(R.id.edtcaracteristica);
        observacionesedt = (EditText) findViewById(R.id.edtobservaciones);
        capitalizalecheck = (CheckBox) findViewById(R.id.checkBoxCap);
       // numinvedt.setText(numinv);

       apiInterface = APIClient.getClient().create(APIInterface.class);

       if(capitalizable == 2) {
           customProgressBar.show(this,"Cargando...");
           Call<Bienes> callinv = apiInterface.doGetBienDetailCAP(numinv);
           callinv.enqueue(new Callback<Bienes>() {
               @Override
               public void onResponse(Call<Bienes> call, Response<Bienes> response) {

                   customProgressBar.dialog.dismiss();
                   Bienes bienes = response.body();
                   idbien = bienes.getIdbien();
                   if(idbien == 0)
                   {
                       Call<Bienes> callinv = apiInterface.doGetBienNuevoDetail(numinv);
                       callinv.enqueue(new Callback<Bienes>() {
                           @Override
                           public void onResponse(Call<Bienes> call, Response<Bienes> response) {

                               customProgressBar.dialog.dismiss();
                               Bienes bienes = response.body();
                               idbien = bienes.getIdbien();
                               if (idbien == 0) {

                                   new AlertDialog.Builder(Detallebien.this)
                                           .setTitle("Bien no registrado")
                                           .setMessage("¿Deseas Añadir este bien?")
                                           .setPositiveButton("Aceptar",new DialogInterface.OnClickListener() {
                                               public void onClick(DialogInterface dialog, int whichButton) {
                                                   Intent view = new Intent(Detallebien.this,ActivityAltaBienK.class);
                                                   view.putExtra("NumInv", numinv);
                                                   view.putExtra("isEdit",0);
                                                   startActivity(view);
                                                   finish();
                                               }
                                           })
                                           .setNegativeButton("Cancelar",new DialogInterface.OnClickListener() {
                                               public void onClick(DialogInterface dialog, int whichButton) {
                                                   finish();
                                               }
                                           })
                                           .show();
                               }

                               marca = bienes.getMarca();
                               caracter = bienes.getCaracteristicas();
                               serie = bienes.getSerie();
                               modelo = bienes.getModelo();
                               fechaalta = bienes.getFechaalta();
                               numinvViejo = bienes.getNuminv();
                               numInvNuevo = bienes.getNuminvNuevo();
                               nofactura = bienes.getNofactura();
                               observaciones = bienes.getObservaciones();
                               capitalizable = bienes.getCapitalizable();
                               EdificioID = bienes.getEdificioID();
                               TipoActivo = bienes.getIdactivo();

                               marcaedt.setText(marca);
                               caracteredt.setText(caracter);
                               serieedt.setText(serie);
                               modeloedt.setText(modelo);
                               fechaaltaedt.setText(fechaalta);
                               numinvedt.setText(numinvViejo);
                               numinvnuevoedt.setText(numInvNuevo);
                               facturaedt.setText(nofactura);
                               observacionesedt.setText(observaciones);

                               if (capitalizable == 1)
                               {
                                     capitalizalecheck.setChecked(true);
                               }
                               else
                               {
                                     capitalizalecheck.setChecked(false);
                               }
                           }

                           @Override
                           public void onFailure(Call<Bienes> call, Throwable t) {
                               customProgressBar.dialog.dismiss();
                           }
                       });

                   }

                   marca = bienes.getMarca();
                   caracter = bienes.getCaracteristicas();
                   serie = bienes.getSerie();
                   modelo = bienes.getModelo();
                   fechaalta = bienes.getFechaalta();
                   numinvViejo = bienes.getNuminv();
                   numInvNuevo = bienes.getNuminvNuevo();
                   nofactura = bienes.getNofactura();
                   observaciones = bienes.getObservaciones();
                   capitalizable = bienes.getCapitalizable();
                   EdificioID = bienes.getEdificioID();
                   TipoActivo = bienes.getIdactivo();

                   marcaedt.setText(marca);
                   caracteredt.setText(caracter);
                   serieedt.setText(serie);
                   modeloedt.setText(modelo);
                   fechaaltaedt.setText(fechaalta);
                   numinvnuevoedt.setText(numInvNuevo);
                   numinvedt.setText(numinvViejo);
                   facturaedt.setText(nofactura);
                   observacionesedt.setText(observaciones);

                   if (capitalizable == 1)
                   {
//                     capitalizabletext.setText("SI");
                       capitalizalecheck.setChecked(true);
                   }
                   else
                   {
//                     capitalizabletext.setText("NO");
                       capitalizalecheck.setChecked(false);
                   }

               }

               @Override
               public void onFailure(Call<Bienes> call, Throwable t) {
                   customProgressBar.dialog.dismiss();
               }
           });
       }
       else {

           customProgressBar.show(this, "Cargando...");
           Call<Bienes> callinv = apiInterface.doGetBienDetail(numinv);
           callinv.enqueue(new Callback<Bienes>() {
               @Override
               public void onResponse(Call<Bienes> call, Response<Bienes> response) {

                   customProgressBar.dialog.dismiss();
                   Bienes bienes = response.body();
                   idbien = bienes.getIdbien();
                   if (idbien == 0) {
                       Call<Bienes> callinv = apiInterface.doGetBienNuevoDetail(numinv);
                       callinv.enqueue(new Callback<Bienes>() {
                           @Override
                           public void onResponse(Call<Bienes> call, Response<Bienes> response) {

                               customProgressBar.dialog.dismiss();
                               Bienes bienes = response.body();
                               idbien = bienes.getIdbien();
                               if (idbien == 0) {

                                   new AlertDialog.Builder(Detallebien.this)
                                           .setTitle("Bien no registrado")
                                           .setMessage("¿Deseas Añadir este bien?")
                                           .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                               public void onClick(DialogInterface dialog, int whichButton) {
                                                   Intent view = new Intent(Detallebien.this, ActivityAltaBienK.class);
                                                   view.putExtra("NumInv", numinv);
                                                   view.putExtra("isEdit", 0);
                                                   startActivity(view);
                                                   finish();
                                               }
                                           })
                                           .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                               public void onClick(DialogInterface dialog, int whichButton) {
                                                   finish();
                                               }
                                           })
                                           .show();
                               }

                               marca = bienes.getMarca();
                               caracter = bienes.getCaracteristicas();
                               serie = bienes.getSerie();
                               modelo = bienes.getModelo();
                               fechaalta = bienes.getFechaalta();
                               numinvViejo = bienes.getNuminv();
                               numInvNuevo = bienes.getNuminvNuevo();
                               nofactura = bienes.getNofactura();
                               observaciones = bienes.getObservaciones();
                               capitalizable = bienes.getCapitalizable();
                               EdificioID = bienes.getEdificioID();
                               TipoActivo = bienes.getIdactivo();

                               marcaedt.setText(marca);
                               caracteredt.setText(caracter);
                               serieedt.setText(serie);
                               modeloedt.setText(modelo);
                               fechaaltaedt.setText(fechaalta);
                               numinvnuevoedt.setText(numInvNuevo);
                               numinvedt.setText(numinvViejo);
                               facturaedt.setText(nofactura);
                               observacionesedt.setText(observaciones);

                               if (capitalizable == 1)
                               {
//                                   capitalizabletext.setText("SI");
                                   capitalizalecheck.setChecked(true);
                               }
                               else
                               {
//                                   capitalizabletext.setText("NO");
                                   capitalizalecheck.setChecked(false);
                               }

                           }

                           @Override
                           public void onFailure(Call<Bienes> call, Throwable t) {
                               customProgressBar.dialog.dismiss();
                           }
                       });

                   }

                   marca = bienes.getMarca();
                   caracter = bienes.getCaracteristicas();
                   serie = bienes.getSerie();
                   modelo = bienes.getModelo();
                   fechaalta = bienes.getFechaalta();
                   numinvViejo = bienes.getNuminv();
                   numInvNuevo = bienes.getNuminvNuevo();
                   nofactura = bienes.getNofactura();
                   observaciones = bienes.getObservaciones();
                   capitalizable = bienes.getCapitalizable();
                   capitalizable = bienes.getCapitalizable();
                   EdificioID = bienes.getEdificioID();
                   TipoActivo = bienes.getIdactivo();

                   marcaedt.setText(marca);
                   caracteredt.setText(caracter);
                   serieedt.setText(serie);
                   modeloedt.setText(modelo);
                   fechaaltaedt.setText(fechaalta);
                   numinvnuevoedt.setText(numInvNuevo);
                   numinvedt.setText(numinvViejo);
                   facturaedt.setText(nofactura);
                   observacionesedt.setText(observaciones);

                   if (capitalizable == 1)
                   {
//                     capitalizabletext.setText("SI");
                       capitalizalecheck.setChecked(true);
                   }
                   else
                   {
//                     capitalizabletext.setText("NO");
                       capitalizalecheck.setChecked(false);
                   }

               }

               @Override
               public void onFailure(Call<Bienes> call, Throwable t) {
                   customProgressBar.dialog.dismiss();
               }
           });
       }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_Edit) {
            Intent view = new Intent(Detallebien.this,ActivityAltaBienK.class);
            view.putExtra("NumInv", numinvViejo);
            view.putExtra("NumInvNew", numInvNuevo);
            view.putExtra("marca",marca);
            view.putExtra("caracter",caracter);
            view.putExtra("serie",serie);
            view.putExtra("modelo",modelo);
            view.putExtra("fecha",fechaalta);
            view.putExtra("nofactura",nofactura);
            view.putExtra("observaciones",observaciones);
            view.putExtra("idbien",idbien);
            view.putExtra("Capitalizable",capitalizable);
            view.putExtra("EdificioID",EdificioID);
            view.putExtra("ActivoID",TipoActivo);
            view.putExtra("isEdit",1);
            startActivity(view);
            finish();
        }
        else
        {

        }
        return super.onOptionsItemSelected(item);
    }


}
