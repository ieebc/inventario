
package com.iee.fotos.inventario;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class TipoActivo {

    @SerializedName("TipoActivos")
    @Expose
    private List<TipoActivo__1> tipoActivos = null;

    public List<TipoActivo__1> getTipoActivos() {
        return tipoActivos;
    }

    public void setTipoActivos(List<TipoActivo__1> tipoActivos) {
        this.tipoActivos = tipoActivos;
    }

}
