package com.iee.fotos.inventario;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Empleado {

    @SerializedName("DepartamentoID")
    @Expose
    private Integer departamentoID;
    @SerializedName("EmpleadosID")
    @Expose
    private Integer empleadosID;
    @SerializedName("Idarea")
    @Expose
    private Integer idarea;
    @SerializedName("noempleado")
    @Expose
    private Integer noempleado;
    @SerializedName("nombre")
    @Expose
    private String nombre;
    @SerializedName("areadesc")
    @Expose
    private String areadesc;
    @SerializedName("deptodesc")
    @Expose
    private String deptodesc;
    @SerializedName("puesto")
    @Expose
    private String puesto;

    public Integer getDepartamentoID() {
        return departamentoID;
    }

    public void setDepartamentoID(Integer departamentoID) {
        this.departamentoID = departamentoID;
    }

    public Integer getEmpleadosID() {
        return empleadosID;
    }

    public void setEmpleadosID(Integer empleadosID) {
        this.empleadosID = empleadosID;
    }

    public Integer getIdarea() {
        return idarea;
    }

    public void setIdarea(Integer idarea) {
        this.idarea = idarea;
    }

    public Integer getNoempleado() {
        return noempleado;
    }

    public void setNoempleado(Integer noempleado) {
        this.noempleado = noempleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAreadesc(){
        return areadesc;
    }

    public void setAreadesc(String areadesc){
        this.areadesc = areadesc;
    }

    public String getDeptodesc(){
        return deptodesc;
    }

    public void setDeptodesc(String deptodesc){
        this.deptodesc = deptodesc;
    }

    public String getPuesto(){
        return puesto;
    }

    public void setPuesto(String puesto){
        this.puesto = puesto;
    }

}
