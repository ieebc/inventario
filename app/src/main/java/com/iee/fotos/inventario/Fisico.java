
package com.iee.fotos.inventario;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Fisico {

    @SerializedName("bienresgs")
    @Expose
    private List<Bienresg> bienresgs = null;

    public List<Bienresg> getBienresgs() {
        return bienresgs;
    }

    public void setBienresgs(List<Bienresg> bienresgs) {
        this.bienresgs = bienresgs;
    }

}
