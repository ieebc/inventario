
package com.iee.fotos.inventario;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bienresg {

    @SerializedName("caracteristicas")
    @Expose
    private String caracteristicas;
    @SerializedName("fechaalta")
    @Expose
    private Object fechaalta;
    @SerializedName("idactivo")
    @Expose
    private Integer idactivo;
    @SerializedName("idbien")
    @Expose
    private Integer idbien;
    @SerializedName("idresguardo")
    @Expose
    private Integer idresguardo;
    @SerializedName("iduser")
    @Expose
    private Integer iduser;
    @SerializedName("is_cap")
    @Expose
    private Integer isCap;
    @SerializedName("is_new")
    @Expose
    private Integer isNew;
    @SerializedName("marca")
    @Expose
    private String marca;
    @SerializedName("modelo")
    @Expose
    private String modelo;
    @SerializedName("nombremp")
    @Expose
    private Object nombremp;
    @SerializedName("numemp")
    @Expose
    private Integer numemp;
    @SerializedName("numinv")
    @Expose
    private String numinv;
    @SerializedName("numinvNuevo")
    @Expose
    private String numinvNuevo;
    @SerializedName("serie")
    @Expose
    private String serie;
    @SerializedName("Estado")
    @Expose
    private int estado;

    public String getCaracteristicas() {
        return caracteristicas;
    }

    public void setCaracteristicas(String caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    public Object getFechaalta() {
        return fechaalta;
    }

    public void setFechaalta(Object fechaalta) {
        this.fechaalta = fechaalta;
    }

    public Integer getIdactivo() {
        return idactivo;
    }

    public void setIdactivo(Integer idactivo) {
        this.idactivo = idactivo;
    }

    public Integer getIdbien() {
        return idbien;
    }

    public void setIdbien(Integer idbien) {
        this.idbien = idbien;
    }

    public Integer getIdresguardo() {
        return idresguardo;
    }

    public void setIdresguardo(Integer idresguardo) {
        this.idresguardo = idresguardo;
    }

    public Integer getIduser() {
        return iduser;
    }

    public void setIduser(Integer iduser) {
        this.iduser = iduser;
    }

    public Integer getIsCap() {
        return isCap;
    }

    public void setIsCap(Integer isCap) {
        this.isCap = isCap;
    }

    public Integer getIsNew() {
        return isNew;
    }

    public void setIsNew(Integer isNew) {
        this.isNew = isNew;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Object getNombremp() {
        return nombremp;
    }

    public void setNombremp(Object nombremp) {
        this.nombremp = nombremp;
    }

    public Integer getNumemp() {
        return numemp;
    }

    public void setNumemp(Integer numemp) {
        this.numemp = numemp;
    }

    public String getNuminv() {
        return numinv;
    }

    public void setNuminv(String numinv) {
        this.numinv = numinv;
    }

    public String getNuminvNuevo() {
        return numinvNuevo;
    }

    public void setNuminvNuevo() {
        this.numinvNuevo = numinvNuevo;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }


}
