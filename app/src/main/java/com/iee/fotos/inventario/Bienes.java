package com.iee.fotos.inventario;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bienes {

    @SerializedName("caracteristicas")
    @Expose
    private String caracteristicas;
    @SerializedName("fechaalta")
    @Expose
    private String fechaalta;
    @SerializedName("idactivo")
    @Expose
    private Integer idactivo;
    @SerializedName("idbien")
    @Expose
    private Integer idbien;
    @SerializedName("iduser")
    @Expose
    private Integer iduser;
    @SerializedName("marca")
    @Expose
    private String marca;
    @SerializedName("modelo")
    @Expose
    private String modelo;
    @SerializedName("nofactura")
    @Expose
    private String nofactura;
    @SerializedName("numinv")
    @Expose
    private String numinv;
    @SerializedName("numinvNuevo")
    @Expose
    private String numinvNuevo;
    @SerializedName("observaciones")
    @Expose
    private String observaciones;
    @SerializedName("serie")
    @Expose
    private String serie;
    @SerializedName("capitalizable")
    @Expose
    private int capitalizable;
    @SerializedName("EdificioID")
    @Expose
    private int EdificioID;

    public String getCaracteristicas() {
        return caracteristicas;
    }

    public void setCaracteristicas(String caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    public String getFechaalta() {
        return fechaalta;
    }

    public void setFechaalta(String fechaalta) {
        this.fechaalta = fechaalta;
    }

    public Integer getIdactivo() {
        return idactivo;
    }

    public void setIdactivo(Integer idactivo) {
        this.idactivo = idactivo;
    }

    public Integer getIdbien() {
        return idbien;
    }

    public void setIdbien(Integer idbien) {
        this.idbien = idbien;
    }

    public Integer getIduser() {
        return iduser;
    }

    public void setIduser(Integer iduser) {
        this.iduser = iduser;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getNofactura() {
        return nofactura;
    }

    public void setNofactura(String nofactura) {
        this.nofactura = nofactura;
    }

    public String getNuminv() {
        return numinv;
    }

    public void setNuminv(String numinv) {
        this.numinv = numinv;
    }

    public String getNuminvNuevo() {
        return numinvNuevo;
    }

    public void setNuminvNuevo(String numinvNuevo) {
        this.numinvNuevo = numinvNuevo;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public int getCapitalizable(){
        return capitalizable;
    }

    public void setCapitalizable(int capitalizable){
        this.capitalizable = capitalizable;
    }

    public int getEdificioID(){
        return EdificioID;
    }

    public void setEdificioID(int edificioID){
        this.EdificioID = edificioID;
    }

}