
package com.iee.fotos.inventario;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BienesList {

    @SerializedName("bienesm")
    @Expose
    private List<Bienesm> bienesm = null;

    public List<Bienesm> getBienesm() {
        return bienesm;
    }

    public void setBienesm(List<Bienesm> bienesm) {
        this.bienesm = bienesm;
    }

}
