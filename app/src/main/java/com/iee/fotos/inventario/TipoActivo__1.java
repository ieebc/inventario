
package com.iee.fotos.inventario;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class TipoActivo__1 {

    @SerializedName("Descripcion")
    @Expose
    private String descripcion;
    @SerializedName("TipoID")
    @Expose
    private Integer tipoID;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getTipoID() {
        return tipoID;
    }

    public void setTipoID(Integer tipoID) {
        this.tipoID = tipoID;
    }

}
