
package com.iee.fotos.inventario;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Resguardos {

    @SerializedName("Resguardos")
    @Expose
    private List<Resguardo> resguardos = null;

    public List<Resguardo> getResguardos() {
        return resguardos;
    }

    public void setResguardos(List<Resguardo> resguardos) {
        this.resguardos = resguardos;
    }

}
