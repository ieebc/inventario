
package com.iee.fotos.inventario;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Edificiom {

    @SerializedName("idedificio")
    @Expose
    private Integer idedificio;
    @SerializedName("nombre")
    @Expose
    private String nombre;

    public Integer getIdedificio() {
        return idedificio;
    }

    public void setIdedificio(Integer idedificio) {
        this.idedificio = idedificio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
