
package com.iee.fotos.inventario;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ArticuloResguardo {

    @SerializedName("BienID")
    @Expose
    private Integer bienID;
    @SerializedName("ID")
    @Expose
    private Integer id;
    @SerializedName("caracteristicas")
    @Expose
    private String caracteristicas;
    @SerializedName("marca")
    @Expose
    private String marca;
    @SerializedName("modelo")
    @Expose
    private String modelo;
    @SerializedName("noinv")
    @Expose
    private String noinv;
    @SerializedName("serie")
    @Expose
    private String serie;

    public Integer getBienID() {
        return bienID;
    }

    public void setBienID(Integer bienID) {
        this.bienID = bienID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCaracteristicas() {
        return caracteristicas;
    }

    public void setCaracteristicas(String caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getNoinv() {
        return noinv;
    }

    public void setNoinv(String noinv) {
        this.noinv = noinv;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

}
