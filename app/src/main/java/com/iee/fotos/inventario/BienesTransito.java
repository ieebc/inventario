
package com.iee.fotos.inventario;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BienesTransito {

    @SerializedName("bienesTransito")
    @Expose
    private List<BienesTransito__1> bienesTransito = null;

    public List<BienesTransito__1> getBienesTransito() {
        return bienesTransito;
    }

    public void setBienesTransito(List<BienesTransito__1> bienesTransito) {
        this.bienesTransito = bienesTransito;
    }

}
