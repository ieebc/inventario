
package com.iee.fotos.inventario;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResguardoDetalles {

    @SerializedName("ArticuloResguardos")
    @Expose
    private List<ArticuloResguardo> articuloResguardos = null;

    public List<ArticuloResguardo> getArticuloResguardos() {
        return articuloResguardos;
    }

    public void setArticuloResguardos(List<ArticuloResguardo> articuloResguardos) {
        this.articuloResguardos = articuloResguardos;
    }

}
