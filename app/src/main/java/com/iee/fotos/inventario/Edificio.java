
package com.iee.fotos.inventario;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Edificio {

    @SerializedName("edificiom")
    @Expose
    private List<Edificiom> edificiom = null;

    public List<Edificiom> getEdificiom() {
        return edificiom;
    }

    public void setEdificiom(List<Edificiom> edificiom) {
        this.edificiom = edificiom;
    }

}
