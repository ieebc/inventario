
package com.iee.fotos.inventario;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BienesTransito__1 {

    @SerializedName("NombreEntrega")
    @Expose
    private String nombreEntrega;
    @SerializedName("NombreRecibe")
    @Expose
    private String nombreRecibe;
    @SerializedName("Observaciones")
    @Expose
    private String observaciones;
    @SerializedName("TransitoID")
    @Expose
    private Integer transitoID;
    @SerializedName("caracteristicas")
    @Expose
    private String caracteristicas;
    @SerializedName("marca")
    @Expose
    private String marca;
    @SerializedName("modelo")
    @Expose
    private String modelo;
    @SerializedName("nuevo")
    @Expose
    private Integer nuevo;
    @SerializedName("numinv")
    @Expose
    private String numinv;
    @SerializedName("numinvViejo")
    @Expose
    private String numinvViejo;
    @SerializedName("serie")
    @Expose
    private String serie;

    public String getNombreEntrega() {
        return nombreEntrega;
    }

    public void setNombreEntrega(String nombreEntrega) {
        this.nombreEntrega = nombreEntrega;
    }

    public String getNombreRecibe() {
        return nombreRecibe;
    }

    public void setNombreRecibe(String nombreRecibe) {
        this.nombreRecibe = nombreRecibe;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Integer getTransitoID() {
        return transitoID;
    }

    public void setTransitoID(Integer transitoID) {
        this.transitoID = transitoID;
    }

    public String getCaracteristicas() {
        return caracteristicas;
    }

    public void setCaracteristicas(String caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Integer getNuevo() {
        return nuevo;
    }

    public void setNuevo(Integer nuevo) {
        this.nuevo = nuevo;
    }

    public String getNuminv() {
        return numinv;
    }

    public void setNuminv(String numinv) {
        this.numinv = numinv;
    }

    public String getNuminvViejo() {
        return numinvViejo;
    }

    public void setNuminvViejo(String numinvViejo) {
        this.numinvViejo = numinvViejo;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

}
