
package com.iee.fotos.inventario;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Resguardo {

    @SerializedName("ResguardoID")
    @Expose
    private Integer resguardoID;
    @SerializedName("areadesc")
    @Expose
    private String areadesc;
    @SerializedName("deptodesc")
    @Expose
    private String deptodesc;
    @SerializedName("estatusresg")
    @Expose
    private String estatusresg;
    @SerializedName("noempleado")
    @Expose
    private Integer noempleado;
    @SerializedName("nombreemp")
    @Expose
    private String nombreemp;

    public Integer getResguardoID() {
        return resguardoID;
    }

    public void setResguardoID(Integer resguardoID) {
        this.resguardoID = resguardoID;
    }

    public String getAreadesc() {
        return areadesc;
    }

    public void setAreadesc(String areadesc) {
        this.areadesc = areadesc;
    }

    public String getDeptodesc() {
        return deptodesc;
    }

    public void setDeptodesc(String deptodesc) {
        this.deptodesc = deptodesc;
    }

    public String getEstatusresg() {
        return estatusresg;
    }

    public void setEstatusresg(String estatusresg) {
        this.estatusresg = estatusresg;
    }

    public Integer getNoempleado() {
        return noempleado;
    }

    public void setNoempleado(Integer noempleado) {
        this.noempleado = noempleado;
    }

    public String getNombreemp() {
        return nombreemp;
    }

    public void setNombreemp(String nombreemp) {
        this.nombreemp = nombreemp;
    }

}
