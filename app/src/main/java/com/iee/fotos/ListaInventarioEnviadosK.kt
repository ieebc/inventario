package com.iee.fotos

import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import com.iee.fotos.inventario.Bienresg
import com.iee.fotos.inventario.Fisico
import com.kroegerama.kaiteki.bcode.BarcodeResultListener
import com.kroegerama.kaiteki.bcode.ui.showBarcodeAlertDialog
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback


class ListaInventarioEnviadosK : AppCompatActivity(){

    lateinit var recyclerview: RecyclerView
    lateinit var textnombre: TextView
    lateinit var madapter: RecyclerviewAdapterInventarioRecibido
    lateinit var mLayoutmanager: RecyclerView.LayoutManager
    lateinit var customprogressbar: CustomProgressBar
    lateinit var noemp: String
    lateinit var apiInterface: APIInterface
    lateinit var fabbarcocde: FloatingActionButton
    lateinit var listainv: List<Bienresg>
    var noinvgl: Int = 0
    var numinv = ""
    var numinvnuevo = ""
    var iscap = 0
    var statusstring = ""
    var EdificioID = 0
    lateinit var datos: SharedPreferences
    var filename = "datos"
    var islogin = ""
    var iduser = 0
    var NombreEmpleado = ""

    companion object {
        private const val TAG = "FragDialogExamples"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_bienes)

        noemp = intent.getStringExtra("noemp").toString()
        EdificioID = intent.getIntExtra("EdificioID",0)
        mLayoutmanager = LinearLayoutManager(this)
        datos = getSharedPreferences(filename, 0)
        islogin = datos.getString("iduser","").toString()
        iduser = islogin.toInt()

        apiInterface = APIClient.getClient().create(APIInterface::class.java)
        recyclerview = findViewById(R.id.recyclerbienes)
        textnombre = findViewById(R.id.textViewEmpleado)
        fabbarcocde = findViewById(R.id.floatingActionButton2)
        fabbarcocde.setImageDrawable(getDrawable(R.drawable.ic_barcode))
        fabbarcocde.visibility = View.INVISIBLE
        textnombre.visibility = View.INVISIBLE

        customprogressbar = CustomProgressBar()
        customprogressbar.show(this,"Cargando...")
        val call = apiInterface.doGetFisicoLista(noemp)
        doAsync{
            call.enqueue(object : retrofit2.Callback<Fisico>{
                override fun onFailure(call: Call<Fisico>?, t: Throwable?) {
                    customprogressbar.dialog.dismiss()
                }

                override fun onResponse(call: Call<Fisico>?, response: Response<Fisico>?) {
                    val result = response!!.body()
                    listainv = result!!.bienresgs
                    recyclerview.setHasFixedSize(true)
                    madapter = RecyclerviewAdapterInventarioRecibido(listainv)
                    recyclerview.post(){
                        recyclerview.layoutManager = mLayoutmanager
                        recyclerview.adapter = madapter
                        customprogressbar.dialog.dismiss()
                    }
                }

            })
        }

    }

    fun ModificarEstadoInception(IdResg: Int)
    {
        doAsync{
            ModificarEstadoInv(IdResg)
        }
    }

    fun ModificarEstadoInv(IdResg: Int)
    {
        runOnUiThread {
            customprogressbar = CustomProgressBar()
            customprogressbar.show(this, "Cargando...")
        }
        val call = apiInterface.doCambiarEstado(IdResg)
        val result = call.execute().body()
        statusstring = result?.status.toString()
        if(statusstring.equals("ok")) {
            customprogressbar.dialog.dismiss()
        }
        else
        {
            customprogressbar.dialog.dismiss()
        }
    }

    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        init {
            execute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
        }
    }

}