package com.iee.fotos

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.iee.fotos.inventario.BienesTransito
import com.iee.fotos.inventario.Resguardo
import com.iee.fotos.inventario.Resguardos
import retrofit2.Call
import retrofit2.Response

class ListaResguardoActivity : AppCompatActivity() {

    lateinit var apiInterface: APIInterface
    lateinit var masbtn : FloatingActionButton
    lateinit var noemptxt: EditText
    lateinit var recyclerResguardo: RecyclerView
    lateinit var Buscarbtn: ImageButton
    lateinit var mLayoutmanager: RecyclerView.LayoutManager
    lateinit var customprogressbar: CustomProgressBar
    lateinit var listaResg : List<Resguardo>
    lateinit var madapter: RecyclerviewAdapterResguardo
    var noempin = 0
    var noemp = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_resguardo)

        mLayoutmanager = LinearLayoutManager(this)
        apiInterface = APIClient.getClient().create(APIInterface::class.java)
        masbtn = findViewById<FloatingActionButton>(R.id.floatingActionButtonAgregar)
        recyclerResguardo = findViewById<RecyclerView>(R.id.RecyclerViewResguardo)
        noemptxt = findViewById<EditText>(R.id.editTextNoemp)
        mLayoutmanager = LinearLayoutManager(this)
        masbtn.setOnClickListener {
            val intent = Intent(this,ResguardoActivity::class.java)
            startActivity(intent)
        }

        Buscarbtn = findViewById<ImageButton>(R.id.imageButton7)
        Buscarbtn.setOnClickListener {
            val noempleado = noemptxt.text.toString()
            if (noempleado.equals("")) {

            }
            else
            {
                noempin = noempleado.toInt()
                llenarRecyclerview(noempin)
            }
        }

    }

    override fun onStart() {
        super.onStart()

    }

    override fun onRestart() {
        super.onRestart()
        llenarRecyclerview(noempin)
    }

    fun llenarRecyclerview(noempeado: Int) {
        customprogressbar = CustomProgressBar()
        customprogressbar.show(this, "Cargando...")
        val call = apiInterface.doGetResguardos(noempeado)
        doAsync{
            call.enqueue(object : retrofit2.Callback<Resguardos> {
                override fun onFailure(call: Call<Resguardos>?, t: Throwable?) {
                    customprogressbar.dialog.dismiss()
                }

                override fun onResponse(call: Call<Resguardos>?, response: Response<Resguardos>?) {
                    val result = response!!.body()
                    listaResg = result!!.resguardos
                    recyclerResguardo.setHasFixedSize(true)
                    madapter = RecyclerviewAdapterResguardo(listaResg)
                    runOnUiThread {
                        recyclerResguardo.layoutManager = mLayoutmanager
                        recyclerResguardo.adapter = madapter
                        customprogressbar.dialog.dismiss()
                    }
                }

            })
        }
    }

    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        init {
            execute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
        }
    }
}