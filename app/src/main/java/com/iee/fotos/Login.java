package com.iee.fotos;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import java.util.Arrays;

public class Login extends AppCompatActivity {

    TextInputEditText usertxt,passwordtxt;
    Button loginbtn;
    String[] arreglousuarios,arreglopasswords;
    String userstrng = "Mario:Arturo:Jesus:Africa:Adriana";
    String passwordstrng = "12345:12345:12345:12345:12345";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        arreglousuarios = userstrng.split(":");
        arreglopasswords = passwordstrng.split(":");

        usertxt = (TextInputEditText) findViewById(R.id.usertxt);
        passwordtxt = (TextInputEditText) findViewById(R.id.passtxt);
        loginbtn = (Button) findViewById(R.id.logibtn);

        String[] permissions = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        Permissions.check(this/*context*/, permissions, null/*rationale*/, null/*options*/, new PermissionHandler() {
            @Override
            public void onGranted() {
                // do your task.
            }
        });

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iniciarsesion();

            }
        });

    }

    public void iniciarsesion(){

        String user = usertxt.getText().toString();
        String pass = passwordtxt.getText().toString();

        if(Arrays.asList(arreglousuarios).contains(user))
        {
            if(Arrays.asList(arreglopasswords).contains(pass))
            {
                Intent view = new Intent(Login.this,MenuActivity.class);
                startActivity(view);
                finish();
            }
            else
            {
                Toast.makeText(this, "Vuelve a intentarlo", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(this, "Vuelve a intentarlo", Toast.LENGTH_SHORT).show();
        }

    }

}
