package com.iee.fotos

import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import com.iee.fotos.inventario.TipoActivo__1
import com.kroegerama.kaiteki.bcode.BarcodeResultListener
import com.kroegerama.kaiteki.bcode.ui.showBarcodeAlertDialog
import java.util.*

class AltaTransitoActivity : AppCompatActivity(),BarcodeResultListener {

    lateinit var editno: EditText
    lateinit var editnonew: EditText
    lateinit var editmarca: EditText
    lateinit var editmodelo: EditText
    lateinit var editserie: EditText
    lateinit var editcaract: EditText
    lateinit var editorb: EditText
    lateinit var apiInterface: APIInterface
    lateinit var statusstring: String
    lateinit var checkcapi : CheckBox
    var bandera = 0
    var marca = ""
    var caracter = ""
    var serie = ""
    var modelo = ""
    var fecha = ""
    var isEdit = 0
    var numinvnuevo = ""
    var nofactura = ""
    var observaciones = ""
    var idbien = 0
    lateinit var datos: SharedPreferences
    var filename = "datos"
    var iduser = ""
    var iduserint = 0
    var factura = 0
    var iscap = 0
    var nombrerecibe = ""
    var nombrenevia = ""
    var idEdificio = 0
    lateinit var nombrearray: Array<String?>
    var adapter: ArrayAdapter<String>? = null
    lateinit var idarray: IntArray
    lateinit var listativos: List<TipoActivo__1>
    lateinit var spinnerActivo : Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alta_transito)

        apiInterface = APIClient.getClient().create(APIInterface::class.java)
        datos = getSharedPreferences(filename, 0)
        iduser = datos.getString("iduser", "").toString()
        iduserint = iduser.toInt()

        val buttoncode: ImageButton = findViewById<ImageButton>(R.id.imageButton5)
        val buttoncodenew: ImageButton = findViewById<ImageButton>(R.id.imageButton6)

        nombrenevia = intent.getStringExtra("nombreEnvia").toString()
        nombrerecibe = intent.getStringExtra("nombreRecibe").toString()
        idEdificio = intent.getIntExtra("EdificioID", 0)

        editno = findViewById<EditText>(R.id.editcode)
        editnonew = findViewById<EditText>(R.id.editcodenew)
        editmarca = findViewById<EditText>(R.id.editmarca)
        editmodelo = findViewById<EditText>(R.id.editmodelo)
        editserie = findViewById<EditText>(R.id.editserie)
        editcaract = findViewById<EditText>(R.id.editcaract)
        editorb = findViewById<EditText>(R.id.editorb)
        var guardarbutton = findViewById<Button>(R.id.buttonaltabien)
        checkcapi = findViewById<CheckBox>(R.id.checkCapi)
        spinnerActivo = findViewById<Spinner>(R.id.spinnerActivo)

        ObtenerTipoActivo()

        buttoncode.setOnClickListener {
            bandera = 1
            showBarcodeAlertDialog(
                    owner = this,
                    listener = this,
                    formats = listOf(BarcodeFormat.CODE_128),
                    barcodeInverted = false
            )
        }

        buttoncodenew.setOnClickListener {
            bandera = 2
            showBarcodeAlertDialog(
                    owner = this,
                    listener = this,
                    formats = listOf(BarcodeFormat.CODE_128),
                    barcodeInverted = false
            )
        }

        guardarbutton.setOnClickListener {
            val idActivo = idarray[spinnerActivo.selectedItemPosition]
            GuardarArticulo(idActivo)
        }
    }

    fun GuardarArticulo(IdActivo: Int){
        val calendar: Calendar = Calendar.getInstance()
        val hour12hrs = calendar[Calendar.HOUR].toString()
        val minutes = calendar[Calendar.MINUTE].toString()
        val hora = "$hour12hrs:$minutes"
        val numinv = editnonew.text.toString()
        val numinvviejo = editno.text.toString()
        val marca = editmarca.text.toString()
        val modelo = editmodelo.text.toString()
        val serie = editserie.text.toString()
        val caracteristicas = editcaract.text.toString()
        val observaciones = editorb.text.toString()
        iscap = if(checkcapi.isChecked)
        {
            1
        }
        else
        {
            2
        }
        doAsync{
            val call = apiInterface.doSetNewAddTransito(idEdificio, iduserint, nombrenevia, nombrerecibe, numinv, numinvviejo, hora, observaciones, marca, modelo, serie, caracteristicas, iscap, IdActivo)
            val result = call.execute().body()
            val statusstring = result?.status.toString()
            if(statusstring == "ok") {
                runOnUiThread {
                    var dialogBuilder = AlertDialog.Builder(this)
                    var layoutView = layoutInflater.inflate(R.layout.dialog_success, null)
                    var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                    dialogBuilder.setView(layoutView)
                    var alertDialog = dialogBuilder.create()
                    alertDialog.show()
                    dialogButton.setOnClickListener {
                        alertDialog.dismiss()
                        finish()
                    }
                }
            }
            else
            {
                runOnUiThread {
                    AlertDialog.Builder(this)
                            .setTitle("Error")
                            .setMessage("A ocurrido un error, vuelve a intentarlo")
                            .setNeutralButton("Aceptar") { dialog, whichButton ->
                                dialog.dismiss()
                            }
                            .show()
                }
            }
        }
    }

    fun ObtenerTipoActivo(){
        doAsync {
            val call = apiInterface.doGetTipoActivos()
            val result = call.execute().body()
            listativos = result!!.tipoActivos

            nombrearray = arrayOfNulls(listativos.size)
            idarray = IntArray(nombrearray.size)
            var indice = nombrearray.size - 1

            for (i in 0..indice) {
                nombrearray[i] = listativos[i].descripcion.toString()
                idarray[i] = listativos[i].tipoID
            }
            runOnUiThread {
                adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, nombrearray)
                adapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinnerActivo.adapter = adapter
            }
        }
    }

    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        init {
            execute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
        }
    }

    override fun onBarcodeScanCancelled() {

    }

    override fun onBarcodeResult(result: Result): Boolean {

        doAsync {
            if (bandera == 1) {
                editno.post() {
                    editno.setText("")
                    editno.setText(result.toString())
                }
            } else {
                editnonew.post() {
                    editnonew.setText("")
                    editnonew.setText(result.toString())
                }
            }
        }
        return true
    }

}