package com.iee.fotos

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.accessibility.AccessibilityEventCompat.setAction
import androidx.lifecycle.Observer
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import com.kroegerama.kaiteki.bcode.BarcodeResultListener
import com.kroegerama.kaiteki.bcode.ui.BarcodeDialog
import com.kroegerama.kaiteki.bcode.ui.showBarcodeAlertDialog
import kotlinx.android.synthetic.main.activity_scanner.*



class ScannerK : AppCompatActivity(), BarcodeResultListener {

    lateinit var codetext: TextView

    override fun onBarcodeScanCancelled() {

    }

    override fun onBarcodeResult(result: Result): Boolean {

        Log.d(TAG, "Result: $result")
        codetext.text = result.toString()
        onBarcodeScanCancelled()
        return true
    }

    companion object {
        private const val TAG = "FragDialogExamples"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanner)

        codetext = findViewById(R.id.textView5) as TextView

        var fabbutton = findViewById(R.id.floatingActionButton) as FloatingActionButton
        fabbutton.setOnClickListener{
            showBarcodeAlertDialog(
                    owner = this,
                    listener = this,
                    formats = listOf(BarcodeFormat.CODE_39,BarcodeFormat.CODABAR,BarcodeFormat.UPC_A,BarcodeFormat.UPC_E,BarcodeFormat.CODE_128,BarcodeFormat.EAN_8,BarcodeFormat.UPC_EAN_EXTENSION),
                    barcodeInverted = false
            )
        }



    }




}