package com.iee.fotos;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.RecyclerView;

import com.iee.fotos.inventario.Edificio;
import com.iee.fotos.inventario.Edificiom;
import com.iee.fotos.inventario.Empleado;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IngresarUsuario extends AppCompatActivity {

    APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
    Spinner spinneredificio;
    EditText editnum;
    Button btnsearch;
    String[] nombrearray;
    int[] idarray;
    CustomProgressBar customProgressBar;
    ArrayAdapter<String> adapter;
    int banderacap = 0;
    String noemp ="";
    String NombreEmpleado = "";
    int IdEdificio = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar_usuario);

        spinneredificio = (Spinner) findViewById(R.id.spinneredificio);
        editnum = (EditText) findViewById(R.id.editnum);
        btnsearch = (Button) findViewById(R.id.buttonsearch);

        customProgressBar = new CustomProgressBar();
        customProgressBar.show(this,"Cargando...");
        Call<Edificio> call1 = apiInterface.doGetEdificioLista();
        call1.enqueue(new Callback<Edificio>() {
            @Override
            public void onResponse(Call<Edificio> call, Response<Edificio> response) {
                Edificio edificio = response.body();
                List<Edificiom> listaedificio = edificio.getEdificiom();

                nombrearray = new String[listaedificio.size()];
                idarray = new int[nombrearray.length];

                for(int i=0;i < nombrearray.length; i++)
                {
                    nombrearray[i] = listaedificio.get(i).getNombre().toString();
                    idarray[i] = listaedificio.get(i).getIdedificio();
                }

                adapter = new ArrayAdapter<String>(IngresarUsuario.this,android.R.layout.simple_spinner_item, nombrearray);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinneredificio.setAdapter(adapter);
                customProgressBar.dialog.dismiss();

            }

            @Override
            public void onFailure(Call<Edificio> call, Throwable t) {
                customProgressBar.dialog.dismiss();
            }
        });


        btnsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                noemp = editnum.getText().toString();
                IdEdificio = idarray[spinneredificio.getSelectedItemPosition()];
                obtenerEmpleado();

            }
        });

    }

    public void  obtenerEmpleado()
    {
        int noempleado = Integer.parseInt(noemp);
        customProgressBar = new CustomProgressBar();
        customProgressBar.show(this,"Cargando...");
        Call<Empleado> call1 = apiInterface.doGetEmpleado(noempleado);
        call1.enqueue(new Callback<Empleado>() {
            @Override
            public void onResponse(Call<Empleado> call, Response<Empleado> response) {
                customProgressBar.dialog.dismiss();
                Empleado empleado = response.body();
                NombreEmpleado = empleado.getNombre().toString();
                Intent view = new Intent(IngresarUsuario.this,ListaInventarioK.class);
                view.putExtra("noemp",noemp);
                view.putExtra("EdificioID", IdEdificio);
                view.putExtra("NombreEmpleado",NombreEmpleado);
                startActivity(view);
            }

            @Override
            public void onFailure(Call<Empleado> call, Throwable t) {
                customProgressBar.dialog.dismiss();
            }
        });
    }

}
