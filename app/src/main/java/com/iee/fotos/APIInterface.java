package com.iee.fotos;

import android.os.AsyncTask;

import com.iee.fotos.inventario.BienStatus;
import com.iee.fotos.inventario.Bienes;
import com.iee.fotos.inventario.BienesList;
import com.iee.fotos.inventario.BienesTransito;
import com.iee.fotos.inventario.Bienesm;
import com.iee.fotos.inventario.Edificio;
import com.iee.fotos.inventario.Empleado;
import com.iee.fotos.inventario.Fisico;
import com.iee.fotos.inventario.ResguardoDetalles;
import com.iee.fotos.inventario.Resguardos;
import com.iee.fotos.inventario.TipoActivo;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {

    @GET("/invapi/Service1.svc/detallebien/")
    Call<Bienes> doGetBienDetail(@Query("numinv") String numinv);

    @GET("/invapi/Service1.svc/detallebienNuevo/")
    Call<Bienes> doGetBienNuevoDetail(@Query("numinv") String numinv);

    @GET("/invapi/Service1.svc/detallebienCAP/")
    Call<Bienes> doGetBienDetailCAP(@Query("numinv") String numinv);

    @GET("/invapi/Service1.svc/obtenerbienes")
    Call<BienesList> doGetBienLista();

    @GET("/invapi/Service1.svc/obtenerbienesCAP")
    Call<BienesList> doGetBienListaCAP();

    @GET("/invapi/Service1.svc/agregarbien/")
    Call<BienStatus> doGetStatusBien(@Query("IDuser") int IDuser,@Query("numinv") String NumInv,@Query("numinvnuevo") String NumInvNuevo,@Query("marca") String marca,@Query("modelo") String modelo, @Query("Serie") String serie,@Query("nofactura") int nofactura,@Query("caracteristicas") String caracteristicas, @Query("observaciones") String observaciones, @Query("capitalizable") int capitalizable, @Query("EdificioID") int EdificioID, @Query("TipoActivo") int TipoActivo);

    @GET("/invapi/Service1.svc/editarbien/")
    Call<BienStatus> doEditStatusBien(@Query("IDbien") int IDbien,@Query("numinv") String NumInv,@Query("numinvnuevo") String NumInvNuevo,@Query("marca") String marca,@Query("modelo") String modelo, @Query("Serie") String serie,@Query("nofactura") int nofactura,@Query("caracteristicas") String caracteristicas, @Query("observaciones") String observaciones, @Query("capitalizable") int capitalizable, @Query("EdificioID") int EdificioID, @Query("TipoActivo") int TipoActivo);

    @GET("/invapi/Service1.svc/sesion/")
    Call<BienStatus> doGetStatusSesion(@Query("username") String username, @Query("password") String password);

    @GET("/invapi/Service1.svc/obteneredificios")
    Call<Edificio> doGetEdificioLista();

    @GET("/invapi/Service1.svc/obtenerinventarioF/")
    Call<Fisico> doGetFisicoLista(@Query("noemp") String noemp);

    @GET("/invapi/Service1.svc/agregarinventario/")
    Call<BienStatus> doSetInventarioF(@Query("noempleado")String noemp,@Query("idbien") int idbien,@Query("idresguardo") int idresg,@Query("isnew") int isnew,@Query("noinv") String noinv,@Query("iscap") int iscap,@Query("noinvnuevo") String noinvnuevo,@Query("EdificioID") int EdificioID,@Query("idusuario") int idusuario);

    @GET("/invapi/Service1.svc/Obtenerempledo/")
    Call<Empleado> doGetEmpleado(@Query("noempleado") int noemp);

    @GET("/invapi/Service1.svc/BorrarInventario/")
    Call<BienStatus> doDeleteInventario(@Query("IdResgFisico") int IdResgFisico);

    @GET("/invapi/Service1.svc/CambiarEstado/")
    Call<BienStatus> doCambiarEstado(@Query("IdResgFisico") int IdResgFisico);

    @GET("/invapi/Service1.svc/ObtenerTransito/")
    Call<BienesTransito> doGetTransito(@Query("IDEdificio") int IDEdificio);

    @GET("/invapi/Service1.svc/SetInvTransito/")
    Call<BienStatus> doGetAddTransito(@Query("IDEdificio") int IDEdificio, @Query("IDUsuario") int IDUsuario,@Query("nombreEntrega") String nombreEntrega,@Query("nombreRecibe") String nombreRecibe,@Query("noInv") String numinv,@Query("horaSalida") String horasalida,@Query("Observaciones") String Observaciones);

    @GET("/invapi/Service1.svc/BorrarTransito/")
    Call<BienStatus> doSetDeleteTransito(@Query("idarticulo") int Idarticulo);

    @GET("/invapi/Service1.svc/ObtenerActivos")
    Call<TipoActivo> doGetTipoActivos();

    @GET("/invapi/Service1.svc/SetInvNuevoTransito/")
    Call<BienStatus> doSetNewAddTransito(@Query("IDEdificio") int IDEdificio, @Query("IDUsuario") int IDUsuario,@Query("nombreEntrega") String nombreEntrega,@Query("nombreRecibe") String nombreRecibe,@Query("noInv") String numinv,@Query("noInvViejo") String numinvviejo,@Query("horaSalida") String horasalida,@Query("Observaciones") String Observaciones,@Query("marca") String marca,@Query("modelo") String modelo,@Query("serie") String serie,@Query("caracteristicas") String caracteristicas,@Query("capitalizable") int capitalizable,@Query("TipoActivo") int TipoActivo);

    @GET("/invapi/Service1.svc/ObtenerTransitoEntregado/")
    Call<BienesTransito> doGetTransitoRecibido(@Query("IDEdificio") int IDEdificio);

    @GET("/invapi/Service1.svc/ModificarTransitoEntregado/")
    Call<BienStatus> doSetTransitoRecibido(@Query("IDEdificio") int IDEdificio,@Query("nombreRecibe") String nombreRecibe,@Query("noInv") String noInv);

    @GET("/invapi/Service1.svc/ObtenerEmpleadoDesc/")
    Call<Empleado> doGetEmpleadoResg(@Query("noempleado") int noemp);

    @GET("/invapi/Service1.svc/ObtenerResguardos/")
    Call<Resguardos> doGetResguardos(@Query("noemp") int noemp);

    @GET("/invapi/Service1.svc/GuardarResguardo/")
    Call<BienStatus> doSetResguardo(@Query("noempleado") int noemp,@Query("nombre") String nombre,@Query("area") String area,@Query("departamento") String departamento,@Query("usuarioid") int usuarioid,@Query("puesto") String puesto);

    @GET("/invapi/Service1.svc/ObtenerResguardoMovilDetalle/")
    Call<ResguardoDetalles> doGetResguardoDetalles(@Query("ResguardoID") int resguardoID);

    @GET("/invapi/Service1.svc/GuardarArticuloResguardo/")
    Call<BienStatus> doSetResguardoDetalle(@Query("resguardoID") int resguardoID, @Query("numinv") String numinv, @Query("usuarioid") int usuarioid);

    @GET("/invapi/Service1.svc/TerminarResguardo/")
    Call<BienStatus> doSetTerminarResguardo(@Query("resguardoID") int resguardoID, @Query("usuarioid") int usuarioid);


}
