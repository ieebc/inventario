package com.iee.fotos

import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import com.iee.fotos.inventario.BienStatus
import com.iee.fotos.inventario.Edificiom
import com.iee.fotos.inventario.TipoActivo__1
import com.kroegerama.kaiteki.bcode.BarcodeResultListener
import com.kroegerama.kaiteki.bcode.ui.showBarcodeAlertDialog


class ActivityAltaBienK : AppCompatActivity(), BarcodeResultListener {

    override fun onBarcodeScanCancelled() {

    }

    override fun onBarcodeResult(result: Result): Boolean {

       // Log.d(MenuActivity.TAG, "Result: $result")
        doAsync{
            if(bandera == 1) {
                editno.post() {
                    editno.setText("")
                    editno.setText(result.toString())
                }
            }
            else
            {
                editnonew.post() {
                    editnonew.setText("")
                    editnonew.setText(result.toString())
                }
            }
        }
        return true
    }

    companion object {
        private const val TAG = "FragDialogExamples"
    }

    lateinit var editno: EditText
    lateinit var editnonew: EditText
    lateinit var editmarca: EditText
    lateinit var editmodelo: EditText
    lateinit var editserie: EditText
    lateinit var editnofactura: EditText
    lateinit var editcaract: EditText
    lateinit var editorb: EditText
    lateinit var apiInterface: APIInterface
    lateinit var status: BienStatus
    lateinit var statusstring: String
    lateinit var checkcapi : CheckBox
    lateinit var SpinnerTipo: Spinner
    lateinit var nombreactivo: Array<String?>
    lateinit var idactivo: IntArray
    lateinit var listativos: List<TipoActivo__1>
    lateinit var nombrearray: Array<String?>
    var adapter: ArrayAdapter<String>? = null
    lateinit var idarray: IntArray
    lateinit var listEdificio: List<Edificiom>
    lateinit var spinnerEdificio: Spinner
    var bandera = 0
    var marca = ""
    var caracter = ""
    var serie = ""
    var modelo = ""
    var fecha = ""
    var isEdit = 0
    var numinvnuevo = ""
    var nofactura = ""
    var observaciones = ""
    var idbien = 0
    lateinit var datos: SharedPreferences
    var filename = "datos"
    var iduser = ""
    var iduserint = 0
    var factura = 0
    var iscap = 0
    var EdificioID = 0
    var ActivoID = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alta_bien)

        apiInterface = APIClient.getClient().create(APIInterface::class.java)
        status = BienStatus()
        statusstring = String()

        datos = getSharedPreferences(filename, 0)
        iduser = datos.getString("iduser", "").toString()
        iduserint = iduser.toInt()

        val buttoncode: ImageButton = findViewById<ImageButton>(R.id.imageButton5)
        val buttoncodenew: ImageButton = findViewById<ImageButton>(R.id.imageButton6)

        editno = findViewById<EditText>(R.id.editcode)
        editnonew = findViewById<EditText>(R.id.editcodenew)
        editmarca = findViewById<EditText>(R.id.editmarca)
        editmodelo = findViewById<EditText>(R.id.editmodelo)
        editserie = findViewById<EditText>(R.id.editserie)
        editnofactura = findViewById<EditText>(R.id.editfactura)
        editcaract = findViewById<EditText>(R.id.editcaract)
        editorb = findViewById<EditText>(R.id.editorb)
        var guardarbutton = findViewById<Button>(R.id.buttonaltabien)
        checkcapi = findViewById<CheckBox>(R.id.checkCapi)
        SpinnerTipo = findViewById(R.id.spinnerActivo2)
        spinnerEdificio = findViewById<Spinner>(R.id.spinnerEdificio2)


        val iin = intent
        val b = iin.extras
        var numinv = ""
        if (b != null) {
            numinv = b.get("NumInv") as String
            isEdit = b.get("isEdit") as Int

            if (isEdit == 1) {

                title = "Editar Bien"

                marca = b.get("marca") as String
                caracter = b.get("caracter") as String
                serie = b.get("serie") as String
                modelo = b.get("modelo") as String
                fecha = b.get("fecha") as String
                numinvnuevo = b.get("NumInvNew") as String
                nofactura = b.get("nofactura") as String
                observaciones = b.getString("observaciones") as String
                idbien = b.get("idbien") as Int
                iscap = b.get("Capitalizable") as Int
                EdificioID = b.getInt("EdificioID")
                ActivoID = b.getInt("ActivoID")

                editno.setText(numinv)
                editmarca.setText(marca)
                editmodelo.setText(modelo)
                editserie.setText(serie)
                editcaract.setText(caracter)
                editnonew.setText(numinvnuevo)
                editnofactura.setText(nofactura)
                editorb.setText(observaciones)



                checkcapi.isChecked = iscap == 1
            }
        }

        editno.setText(numinv)

        buttoncode.setOnClickListener {
            bandera = 1
                showBarcodeAlertDialog(
                        owner = this,
                        listener = this,
                        formats = listOf(BarcodeFormat.CODE_128),
                        barcodeInverted = false
                )
        }

        buttoncodenew.setOnClickListener {
            bandera = 2
            showBarcodeAlertDialog(
                    owner = this,
                    listener = this,
                    formats = listOf(BarcodeFormat.CODE_128),
                    barcodeInverted = false
            )
        }

        guardarbutton.setOnClickListener {

            val numerocodigo = editno.text.toString()
            val numinvnuevo = editnonew.text.toString()
            val marca = editmarca.text.toString()
            val modelo = editmodelo.text.toString()
            val serie = editserie.text.toString()
            val idedificio = idarray[spinnerEdificio.selectedItemPosition]
            val idactivo = idactivo[SpinnerTipo.selectedItemPosition]
            if (editnofactura.text.isEmpty()) {
                factura = 0
            }
            else
            {
                factura = editnofactura.text.toString().toInt()
            }
            val caracteristica = editcaract.text.toString()
            val observacion = editorb.text.toString()
            if(checkcapi.isChecked)
            {
                iscap = 1
            }
            else
            {
                iscap = 2
            }

            if (isEdit == 0) {
                doAsync{
                    val call = apiInterface.doGetStatusBien(iduserint, numerocodigo, numinvnuevo, marca, modelo, serie, factura, caracteristica, observacion, iscap, idedificio, idactivo)
                    val result = call.execute().body()
                    // status.status = result?.status.toString()
                    statusstring = result?.status.toString()

                    guardarbutton.post(){
                        if (statusstring.equals("ok"))
                        {
                            var  dialogBuilder = AlertDialog.Builder(this@ActivityAltaBienK)
                            var layoutView = layoutInflater.inflate(R.layout.dialog_success, null)
                            var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                            dialogBuilder.setView(layoutView)
                            var   alertDialog = dialogBuilder.create()
                            alertDialog.show()
                            dialogButton.setOnClickListener {
                                alertDialog.dismiss()
                                finish()
                            }
                        }
                        else
                        {

                        }
                    }

                }
            }
            else
            {
                doAsync{
                    val call = apiInterface.doEditStatusBien(idbien, numerocodigo, numinvnuevo, marca, modelo, serie, factura, caracteristica, observacion, iscap, idedificio,idactivo)
                    val result = call.execute().body()
                    // status.status = result?.status.toString()
                    statusstring = result?.status.toString()

                    guardarbutton.post(){
                        if (statusstring.equals("ok"))
                        {

                            var  dialogBuilder = AlertDialog.Builder(this@ActivityAltaBienK)
                            var layoutView = layoutInflater.inflate(R.layout.dialog_success, null)
                            var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                            dialogBuilder.setView(layoutView)
                            var   alertDialog = dialogBuilder.create()
                            alertDialog.show()
                            dialogButton.setOnClickListener {
                                alertDialog.dismiss()
                                finish()
                            }
                        }
                        else
                        {

                        }
                    }

                }
            }
        }

    }

    override fun onStart() {
        super.onStart()
        ObtenerEdificios()
        ObtenerTipoActivo()
    }


    fun ObtenerEdificios(){
        doAsync {
            val call = apiInterface.doGetEdificioLista()
            val result = call.execute().body()
            listEdificio = result!!.edificiom

            nombrearray = arrayOfNulls(listEdificio.size)
            idarray = IntArray(nombrearray.size)
            var indice = nombrearray.size - 1

            for (i in 0..indice) {
                nombrearray[i] = listEdificio[i].nombre.toString()
                idarray[i] = listEdificio[i].idedificio
            }
            runOnUiThread {
                adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, nombrearray)
                adapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinnerEdificio.adapter = adapter
            }
            if (isEdit == 1){
                PosicionarEdificio(idarray,EdificioID)
            }
        }
    }

    fun ObtenerTipoActivo(){
        AltaTransitoActivity.doAsync {
            val call = apiInterface.doGetTipoActivos()
            val result = call.execute().body()
            listativos = result!!.tipoActivos

            nombreactivo = arrayOfNulls(listativos.size)
            idactivo = IntArray(nombreactivo.size)
            var indice = nombreactivo.size - 1

            for (i in 0..indice) {
                nombreactivo[i] = listativos[i].descripcion.toString()
                idactivo[i] = listativos[i].tipoID
            }
            runOnUiThread {
                adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, nombreactivo)
                adapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                SpinnerTipo.adapter = adapter
            }
            if(isEdit == 1){
                PosicionarTipoActivo(idactivo,ActivoID)
            }
        }
    }

    private fun PosicionarEdificio(EdificioArray : IntArray, EdifioID : Int)
    {
        var index = 0
        for(i in 0 until EdificioArray.size){
            if (EdificioArray[i] == EdifioID)
            {
                index = i
            }
        }
        runOnUiThread {
            spinnerEdificio.setSelection(index)
        }

    }

    private fun PosicionarTipoActivo(TipoArray: IntArray, TipoID : Int){
        var index = 0
        for(i in 0 until TipoArray.size){
            if (TipoArray[i] == TipoID)
            {
                index = i
            }
        }
        runOnUiThread {
            SpinnerTipo.setSelection(index)
        }
    }

    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        init {
            execute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
        }
    }

}