package com.iee.fotos

import android.content.Intent
import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.graphics.drawable.ColorDrawable
import android.os.AsyncTask
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import com.iee.fotos.inventario.ArticuloResguardo
import com.iee.fotos.inventario.BienesTransito__1
import com.iee.fotos.inventario.ResguardoDetalles
import com.iee.fotos.inventario.Resguardos
import com.kroegerama.kaiteki.bcode.BarcodeResultListener
import com.kroegerama.kaiteki.bcode.ui.showBarcodeAlertDialog
import org.w3c.dom.Text
import retrofit2.Call
import retrofit2.Response

class ListaDetalleResguardo : AppCompatActivity(), BarcodeResultListener {

    lateinit var apiInterface: APIInterface
    lateinit var nombretxt: TextView
    lateinit var deptotxt: TextView
    lateinit var edificiotxt: TextView
    lateinit var terminarbtn: Button
    lateinit var Scanbtn: FloatingActionButton
    var nombre = ""
    var area = ""
    var depto = ""
    var IDResguardo = 0
    lateinit var Recycler: RecyclerView
    lateinit var mLayoutmanager: RecyclerView.LayoutManager
    lateinit var madapter: RecyclerviewAdapterResguardoDetalle
    lateinit var customprogressbar: CustomProgressBar
    lateinit var listainv: List<ArticuloResguardo>
    lateinit var datos: SharedPreferences
    var filename = "datos"
    var iduser = ""
    var iduserint = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resguardo_detalle)

        nombre = intent.getStringExtra("nombre").toString()
        area = intent.getStringExtra("area").toString()
        depto = intent.getStringExtra("depto").toString()
        IDResguardo = intent.getIntExtra("ResguardoID",0)

        datos = getSharedPreferences(filename, 0)
        iduser = datos.getString("iduser", "").toString()
        iduserint = iduser.toInt()

        mLayoutmanager = LinearLayoutManager(this)
        apiInterface = APIClient.getClient().create(APIInterface::class.java)

        nombretxt = findViewById<TextView>(R.id.textView37)
        deptotxt = findViewById<TextView>(R.id.textView40)
        edificiotxt = findViewById<TextView>(R.id.textView41)
        terminarbtn = findViewById<Button>(R.id.button6)
        Scanbtn = findViewById<FloatingActionButton>(R.id.floatingActionButton4)
        Recycler = findViewById<RecyclerView>(R.id.RecyclerDetalleResg)

        nombretxt.text = "Nombre: $nombre"
        edificiotxt.text = "Area: $area"
        deptotxt.text = "Departamento: $depto"

        Scanbtn.setOnClickListener {
            showBarcodeAlertDialog(
                    owner = this,
                    listener = this,
                    formats = listOf(BarcodeFormat.CODE_128),
                    barcodeInverted = false
            )
        }

        terminarbtn.setOnClickListener {
            TerminarResguardoMovil(IDResguardo,iduserint)
        }


    }

    override fun onStart() {
        super.onStart()
        llenarRecyclerview(IDResguardo)
    }

    override fun onRestart() {
        super.onRestart()
    }

    fun llenarRecyclerview(ResguardoID: Int) {
        customprogressbar = CustomProgressBar()
        customprogressbar.show(this, "Cargando...")
        val call = apiInterface.doGetResguardoDetalles(ResguardoID)
        doAsync{
            call.enqueue(object : retrofit2.Callback<ResguardoDetalles> {
                override fun onFailure(call: Call<ResguardoDetalles>?, t: Throwable?) {
                    customprogressbar.dialog.dismiss()
                }

                override fun onResponse(call: Call<ResguardoDetalles>?, response: Response<ResguardoDetalles>?) {
                    val result = response!!.body()
                    listainv = result!!.articuloResguardos
                    Recycler.setHasFixedSize(true)
                    madapter = RecyclerviewAdapterResguardoDetalle(listainv)
                    runOnUiThread {
                        Recycler.layoutManager = mLayoutmanager
                        Recycler.adapter = madapter
                        customprogressbar.dialog.dismiss()
                    }
                }

            })
        }

    }

    fun GuardarDetalle(resguardoid: Int, noinv: String, usuarioid : Int){
        doAsync{
            val call = apiInterface.doSetResguardoDetalle(resguardoid,noinv,usuarioid)
            val result = call.execute().body()
            val statusstring = result?.status.toString()
            if(statusstring.equals("ok")) {
                runOnUiThread {
                    var dialogBuilder = AlertDialog.Builder(this)
                    var layoutView = layoutInflater.inflate(R.layout.dialog_success, null)
                    var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                    dialogBuilder.setView(layoutView)
                    var alertDialog = dialogBuilder.create()
                    alertDialog.show()
                    dialogButton.setOnClickListener {
                        alertDialog.dismiss()
                        llenarRecyclerview(resguardoid)
                    }
                }
            }
            else if (statusstring.equals("Error"))
            {
                runOnUiThread {
                    var dialogBuilder = AlertDialog.Builder(this)
                    var layoutView = layoutInflater.inflate(R.layout.activity_dialog_noexiste, null)
                    var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                    dialogBuilder.setView(layoutView)
                    var alertDialog = dialogBuilder.create()
                    alertDialog.show()
                    dialogButton.setOnClickListener {
                        alertDialog.dismiss()
                    }
                }
            }
        }

    }

    fun TerminarResguardoMovil(ResguardoID: Int,usuarioid: Int)
    {
        doAsync{
            val call = apiInterface.doSetTerminarResguardo(ResguardoID,usuarioid)
            val result = call.execute().body()
            val statusstring = result?.status.toString()
            if(statusstring.equals("ok")) {
                runOnUiThread {
                    var dialogBuilder = AlertDialog.Builder(this)
                    var layoutView = layoutInflater.inflate(R.layout.dialog_terminado, null)
                    var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                    dialogBuilder.setView(layoutView)
                    var alertDialog = dialogBuilder.create()
                    alertDialog.show()
                    dialogButton.setOnClickListener {
                        alertDialog.dismiss()
                        finish()
                    }
                }
            }
            else if (statusstring.equals("Error"))
            {
                runOnUiThread {
                    var dialogBuilder = AlertDialog.Builder(this)
                    var layoutView = layoutInflater.inflate(R.layout.dialog_error, null)
                    var dialogButton = layoutView.findViewById(R.id.btnDialog) as Button
                    dialogBuilder.setView(layoutView)
                    var alertDialog = dialogBuilder.create()
                    alertDialog.show()
                    dialogButton.setOnClickListener {
                        alertDialog.dismiss()
                    }
                }
            }
        }
    }

    override fun onBarcodeScanCancelled() {

    }

    override fun onBarcodeResult(result: Result): Boolean {
        GuardarDetalle(IDResguardo,result.toString(),iduserint)
        return true
    }

    class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        init {
            execute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
        }
    }

}